<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\register;
use Illuminate\Support\Facades\Input;
use App\Models\enquiry;
use App\Models\adminroles;
use App\Models\nativity;
use App\Models\brand;
use App\Models\menu;
use App\Models\subcategory;
use App\Models\vendor;
use App\Models\product;
use App\Models\area;
use App\Models\Customer;
use App\Models\baselocation;
use App\Models\productcategory;
use App\Models\mothertongue;
use App\Models\addressproof;
use App\Models\photoproof;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Hash;
use Crypt;
use Auth;
use File;
use DB;

class apiController extends Controller
{
	public function storeuser()
	{
		
		$name= Input::get('name');
		$email=Input::get('email');
		$password=Input::get('password');
		$register= new register();

		$register->name = $name;
		$register->email = $email;
		// $register->password = $password;
		
		////////////
		$key = 'password to (en/de)crypt';
		$encrypted_password = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $password, MCRYPT_MODE_CBC, md5(md5($key))));
		$register->password = $encrypted_password;
		////////////
		$register->save();

		if(isset($register->id))
		{
			$response = array('status' => 'success','message'=>'data save successfuly' );
		}
		else
		{
			$response = array('status' => 'error','message'=>'data save Failed' );
		}
		return json_encode($response);
	}

	public function userlogin()
	{
		$email=Input::get('email');
		$password=Input::get('password');
////////////
		$key = 'password to (en/de)crypt';
		$encrypted_password = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $password, MCRYPT_MODE_CBC, md5(md5($key))));
///////////
		$user=register::where('email','=',$email)
		->where('password','=',$encrypted_password)
		->first();

		if(isset($user))
		{
			$response = array('status' => 'success','message'=>'login successfully' );
		}
		else
		{
			$response = array('status' => 'error','message'=>'can log in properly' );
		}

		return json_encode($response);
	}

	public function userlist()
	{
		$result=register::select(array('name', 'email'))->get();

		return json_encode($result);
	}

	public function enquiry()
	{
		$id=Input::get('id');

		if(isset($id))
		{

			$enquiry = enquiry::find($id);
			$name=  Input::get('name');
			$phone= Input::get('phone');
			$location=Input::get('location');
			$email= Input::get('email');
			$source= Input::get('source');
			$product= Input::get('product');
			$remarks= Input::get('remarks');

			$enquiry->name=$name;
			$enquiry->phone=$phone;
			$enquiry->location=$location;
			$enquiry->email=$email;
			$enquiry->source=$source;
			$enquiry->product=$product;
			$enquiry->remarks=$remarks;
			$enquiry->status=0;
			$enquiry->save();
		}
		else{
			$name=  Input::get('name');
			$phone= Input::get('phone');
			$location= Input::get('location');
			$email= Input::get('email');
			$source= Input::get('source');
			$product= Input::get('product');
			$remarks= Input::get('remarks');

			$enquiry= new enquiry();

			$enquiry->name=$name;
			$enquiry->phone=$phone;
			$enquiry->location=$location;
			$enquiry->email=$email;
			$enquiry->source=$source;
			$enquiry->product=$product;
			$enquiry->remarks=$remarks;
			$enquiry->status=0;
			$enquiry->save();
		}
		if(isset($enquiry->id))
		{
			$response = array('status' => 'success','message'=>'enquiry save successfully' );
		}
		else
		{
			$response = array('status' => 'error','message'=>'enquiry Failure' );
		}
		return json_encode($response);
	}

	public function  enquirylist()
	{
		$result=enquiry::all();
		return json_encode($result);
	}

	public function  enquiryedit()
	{
		$id =Input::get('id');
		$data = enquiry::find($id);

		if(isset($data))
		{
			$result = array('status'=>'success','message'=>'Fetch data successfully','data'=>$data);
		}
		else
		{
			$result = array('status'=>'error','message'=>'no record found');
		}
		return  json_encode($result);
	}

	public function adminrole()
	{
		$id=Input::get('id');
		if(isset($id))
		{
			$adminrole = adminroles::find($id);
			$role=Input::get('role');
			$adminrole->role=$role;
			$adminrole->save();
		}
		else
		{
			$role =  Input::get('role');
			$status='1';
			$adminrole = new adminroles();
			$adminrole->role=$role;
			$adminrole->status=$status;
			$adminrole->save();
		}
		if(isset($adminrole->id))
		{
			$response = array('status' => 'success','message'=>'enquiry save successfully' );
		}
		else
		{
			$response = array('status' => 'error','message'=>'enquiry Failure' );
		}
		return json_encode($response);
	}

	public function  rolelist()
	{
		$result=adminroles::all();
		return json_encode($result);
	}

	public function menulist()
	{
		$menu = menu::where('status','=',1)
		->where('parent_id','=',0)
		->where('id','!=',17)
		->get();

		if(isset($menu))
		{
			$response = array('status' => 'success','message'=>'menu successfully','data' => $menu );
		}
		else
		{
			$response = array('status' => 'error','message'=>'menu  failure' );
		}
		return json_encode($response);
		/*	return view('layout.apimenu',compact('menu','sub_menu'));	*/
	}
	public function submenulist()
	{
		$parent_id = input::get('parent_id');
		$sub_menu = menu::where('menu.parent_id','=',$parent_id)
		->where('parent_id','!=',17)
		->select('menu.menu_name','menu.id','menu.parent_id','menu.url','menu.status')
		->get();

		if(isset($sub_menu))
		{
			$response = array('status' => 'success','message'=>'menu successfully','data' => $sub_menu );
		}
		else
		{
			$response = array('status' => 'error','message'=>'menu  failure' );
		}
		return json_encode($response);
	}
	//<!--------------------------------------------Add,view and edit master menu----------------------------!>
	public function masterdata_productcategory()
	{
		$id=Input::get('id');
		if(isset($id))
		{
			$pcategory = productcategory::find($id);
			$productcategory=Input::get('productcategory');
			$pcategory->productcategory=$productcategory;
			$pcategory->save();
		}
		else
		{
			$productcategory =  Input::get('productcategory');
			$status='1';
			$created_by='';
			$modified_by='';
			$pcategory = new productcategory();
			$pcategory->productcategory=$productcategory;
			$pcategory->status=$status;
			$pcategory->created_by=$created_by;
			$pcategory->modified_by=$modified_by;
			$pcategory->save();
		}
		if(isset($pcategory->id))
		{
			$response = array('status' => 'success','message'=>'productcategory save successfully' );
		}
		else
		{
			$response = array('status' => 'error','message'=>'productcategory Failure' );
		}
		return json_encode($response);
	}

	public function  view_productcategory()
	{
		$result=productcategory::all();
		return json_encode($result);
	}

	public function  productcategoryedit()
	{
		$id =Input::get('id');
		$data = productcategory::find($id);
		if(isset($data))
		{
			$result = array('status'=>'success','message'=>'Fetch data successfully','data'=>$data);
		}
		else
		{
			$result = array('status'=>'error','message'=>'no record found');
		}
		return  json_encode($result);
	}

	public function masterdata_subcategory()
	{
		$id = Input::get('id');

		if(isset($id))
		{
			$scategory = subcategory::find($id);
			$subcategory = Input::get('subcategory');
			$scategory->subcategory = $subcategory;
			$scategory->maincategory = Input::get('maincategory');
			$scategory->save();
		}
		else
		{
			$subcategory =  Input::get('subcategory');
			$status='1';
			$created_by='';
			$modified_by='';
			$scategory = new subcategory();
			$scategory->maincategory = Input::get('maincategory');
			$scategory->subcategory=$subcategory;
			$scategory->status=$status;
			$scategory->created_by=$created_by;
			$scategory->modified_by=$modified_by;
			$scategory->save();
		}

		if(isset($scategory->id))
		{
			$response = array('status' => 'success','message'=>'subcategory save successfully' );
		}
		else
		{
			$response = array('status' => 'error','message'=>'subcategory Failure' );
		}
		return json_encode($response);
	}

	public function  view_subcategory()
	{
		$result=subcategory::all();
		return json_encode($result);
	}

	public function  subcategoryedit()
	{
		$id =Input::get('id');
		$data = subcategory::find($id);
		if(isset($data))
		{
			$result = array('status'=>'success','message'=>'Fetch data successfully','data'=>$data);
		}
		else
		{
			$result = array('status'=>'error','message'=>'no record found');
		}
		return  json_encode($result);
	}

	public function  sub_category_list()
	{
		$id =Input::get('productcat_id');
		$data = subcategory::where('maincategory','=',$id)->where('status','=',1)->get();
		if(isset($data))
		{
			$result = array('status'=>'success','message'=>'Fetch data successfully','data'=>$data);
		}
		else
		{
			$result = array('status'=>'error','message'=>'no record found');
		}
		return  json_encode($result);
	}	

	public function masterdata_vendor()
	{
		$id=Input::get('id');
		if(isset($id))
		{
			$vendor = vendor::find($id);
			$vendorname=Input::get('vendorname');
			$vendor->vendorname=$vendorname;
			$vendor->save();
		}
		else
		{
			$vendorname = Input::get('vendorname');
			$status='1';
			$created_by='';
			$modified_by='';
			$vendor = new vendor();
			$vendor->vendorname=$vendorname;
			$vendor->status=$status;
			$vendor->created_by=$created_by;
			$vendor->modified_by=$modified_by;
			$vendor->save();
		}
		if(isset($vendor->id))
		{
			$response = array('status' => 'success','message'=>'vendor save successfully' );
		}
		else
		{
			$response = array('status' => 'error','message'=>'vendor Failure' );
		}
		return json_encode($response);
	}

	public function  view_vendor()
	{
		$result=vendor::all();
		return json_encode($result);
	}

	public function  vendoredit()
	{
		$id =Input::get('id');
		$data = vendor::find($id);
		if(isset($data))
		{
			$result = array('status'=>'success','message'=>'Fetch data successfully','data'=>$data);
		}
		else
		{
			$result = array('status'=>'error','message'=>'no record found');
		}
		return  json_encode($result);
	}	
	public function masterdata_brand()
	{
		$id=Input::get('id');
		if(isset($id))
		{
			$brand = brand::find($id);
			$brandname=Input::get('brand');
			$brand->brand=$brandname;
			$brand->save();
		}
		else
		{
			$brandname =  Input::get('brand');
			$status='1';
			$created_by='';
			$modified_by='';
			$brand = new brand();
			$brand->brand=$brandname;
			$brand->status=$status;
			$brand->created_by=$created_by;
			$brand->modified_by=$modified_by;
			$brand->save();
		}
		if(isset($brand->id))
		{
			$response = array('status' => 'success','message'=>'brand save successfully' );
		}
		else
		{
			$response = array('status' => 'error','message'=>'brand Failure' );
		}
		return json_encode($response);
	}

	public function  view_brand()
	{
		$result=brand::all();

		return json_encode($result);
	}
	public function brandedit()
	{
		$id =Input::get('id');
		$data = brand::find($id);
		if(isset($data))
		{
			$result = array('status'=>'success','message'=>'Fetch data successfully','data'=>$data);
		}
		else
		{
			$result = array('status'=>'error','message'=>'no record found');
		}
		return  json_encode($result);
	}	

	public function masterdata_nativity()
	{
		$id=Input::get('id');
		if(isset($id))
		{
			$nativity = nativity::find($id);
			$native =Input::get('nativitylanguage');
			$nativity->nativitylanguage=$native;
			$nativity->save();
		}

		else
		{
			$native = Input::get('nativitylanguage');
			$status='1';
			$created_by='';
			$modified_by='';
			$nativity = new nativity();
			$nativity->nativitylanguage=$native;
			$nativity->status=$status;
			$nativity->created_by=$created_by;
			$nativity->modified_by=$modified_by;
			$nativity->save();
		}

		if(isset($nativity->id))
		{
			$response = array('status' => 'success','message'=>'native save successfully' );
		}

		else
		{
			$response = array('status' => 'error','message'=>'native Failure' );

		}

		return json_encode($response);
	}
	public function  view_nativity()
	{
		$result=nativity::all();

		return json_encode($result);
	}

	public function nativityedit()
	{

		$id =Input::get('id');
		$data = nativity::find($id);

		if(isset($data))
		{
			$result = array('status'=>'success','message'=>'Fetch data successfully','data'=>$data);
		}
		else
		{
			$result = array('status'=>'error','message'=>'no record found');
		}

		return  json_encode($result);

	}

	public function masterdata_area()
	{
		$id=Input::get('id');
		if(isset($id))
		{
			$area = area::find($id);
			$area_name = Input::get('areaname');
			$area->areaname=$area_name;
			$area->save();
		}
		else
		{
			$area_name = Input::get('areaname');
			$status='1';
			$created_by='';
			$modified_by='';
			$area = new area();
			$area->areaname=$area_name;
			$area->status=$status;
			$area->created_by=$created_by;
			$area->modified_by=$modified_by;
			$area->save();
		}

		if(isset($area->id))
		{
			$response = array('status' => 'success','message'=>'area save successfully' );
		}

		else
		{
			$response = array('status' => 'error','message'=>'area Failure' );

		}

		return json_encode($response);
	}
	public function view_area()
	{
		$result=area::all();

		return json_encode($result);
	}

	public function areaedit()
	{

		$id =Input::get('id');
		$data = area::find($id);

		if(isset($data))
		{
			$result = array('status'=>'success','message'=>'Fetch data successfully','data'=>$data);
		}
		else
		{
			$result = array('status'=>'error','message'=>'no record found');
		}

		return  json_encode($result);

	}	

	public function masterdata_baselocation()
	{
		$id=Input::get('id');
		if(isset($id))
		{
			$baselocation = baselocation::find($id);
			$base=Input::get('baselocation');
			$baselocation->baselocation=$base;
			$baselocation->save();
		}
		else
		{
			$baselocationname = Input::get('baselocation');
			$status='1';
			$created_by='';
			$modified_by='';
			$baselocation = new baselocation();
			$baselocation->baselocation=$baselocationname;
			$baselocation->status=$status;
			$baselocation->created_by=$created_by;
			$baselocation->modified_by=$modified_by;
			$baselocation->save();
		}
		if(isset($baselocation->id))
		{
			$response = array('status' => 'success','message'=>'baselocation save successfully' );
		}
		else
		{
			$response = array('status' => 'error','message'=>'baselocation Failure' );
		}
		return json_encode($response);
	}

	public function  view_baselocation()
	{
		$result=baselocation::all();

		return json_encode($result);
	}

	public function baselocationedit()
	{
		$id =Input::get('id');
		$data = baselocation::find($id);

		if(isset($data))
		{
			$result = array('status'=>'success','message'=>'Fetch data successfully','data'=>$data);
		}
		else
		{
			$result = array('status'=>'error','message'=>'no record found');
		}
		return  json_encode($result);
	}	

	public function masterdata_mothertongue()
	{
		$id=Input::get('id');
		if(isset($id))
		{
			$mother = mothertongue::find($id);
			$mother=Input::get('mothertongue');
			$mother->mothertongue=$mother;
			$mother->save();
		}

		else
		{
			$mother = Input::get('mothertongue');
			$status='1';
			$created_by='';
			$modified_by='';
			$mothertongue = new mothertongue();
			$mothertongue->mothertongue=$mother;
			$mothertongue->status=$status;
			$mothertongue->created_by=$created_by;
			$mothertongue->modified_by=$modified_by;
			$mothertongue->save();
		}

		if(isset($mothertongue->id))
		{
			$response = array('status' => 'success','message'=>'brand save successfully' );
		}
		else
		{
			$response = array('status' => 'error','message'=>'brand Failure' );
		}
		return json_encode($response);
	}

	public function  view_mothertongue()
	{
		$result=mothertongue::all();
		return json_encode($result);
	}

	public function mothertonguedit()
	{
		$id =Input::get('id');
		$data = mothertongue::find($id);
		if(isset($data))
		{
			$result = array('status'=>'success','message'=>'Fetch data successfully','data'=>$data);
		}
		else
		{
			$result = array('status'=>'error','message'=>'no record found');
		}
		return  json_encode($result);
	}	

public function masterdata_addressproof()
	{
		$id=Input::get('id');
		if(isset($id))
		{
			$address = addressproof::find($id);
			$address=Input::get('addressproof_name');
			$address->addressproof_name=$address;
			$address->save();
		}

		else
		{
			$addressproof = Input::get('addressproof_name');
			$status='1';
			$created_by='';
			$modified_by='';
			$address = new addressproof();
			$address->addressproof_name=$addressproof;
			$address->status=$status;
			$address->created_by=$created_by;
			$address->modified_by=$modified_by;
			$address->save();
		}

		if(isset($address->id))
		{
			$response = array('status' => 'success','message'=>'Address proof save successfully' );
		}
		else
		{
			$response = array('status' => 'error','message'=>'Address proof Failure' );
		}
		return json_encode($response);
	}

	public function  view_addressproof()
	{
		$result=addressproof::all();

		return json_encode($result);
	}

	public function addressproofedit()
	{
		$id =Input::get('id');
		$data = addressproof::find($id);
		if(isset($data))
		{
			$result = array('status'=>'success','message'=>'Fetch data successfully','data'=>$data);
		}
		else
		{
			$result = array('status'=>'error','message'=>'no record found');
		}
		return  json_encode($result);
	}

	public function masterdata_photoproof()
	{
		$id=Input::get('id');
		if(isset($id))
		{
			$photo = photoproof::find($id);
			$photo_name=Input::get('photoproof_name');
			$photo->photoproof_name=$photo_name;
			$photo->save();
		}

		else
		{
			$photo_name = Input::get('photoproof_name');
			$status='1';
			$created_by='';
			$modified_by='';
			$photo = new photoproof();
			$photo->photoproof_name=$photo_name;
			$photo->status=$status;
			$photo->created_by=$created_by;
			$photo->modified_by=$modified_by;
			$photo->save();
		}

		if(isset($photo->id))
		{
			$response = array('status' => 'success','message'=>'Photo proof save successfully' );
		}
		else
		{
			$response = array('status' => 'error','message'=>'photo proof Failure' );
		}
		return json_encode($response);
	}

	public function  view_photoproof()
	{
		$result=photoproof::all();

		return json_encode($result);
	}

	public function photoproofedit()
	{
		$id =Input::get('id');
		$data = photoproof::find($id);
		if(isset($data))
		{
			$result = array('status'=>'success','message'=>'Fetch data successfully','data'=>$data);
		}
		else
		{
			$result = array('status'=>'error','message'=>'no record found');
		}
		return  json_encode($result);
	}
//<!--------------------------------------------End of matser menu----------------------------!>

	public function add_product() //add new product
	{
		$ss=input::all();
		$yr=date('Y');
		$month=date('m');
		$invoice_path = "Uploads/Invoice/".$yr."/".$month;
		$image_path = "Uploads/Product Image/".$yr."/".$month;
		$qrcode_path = "Uploads/QRCode/".$yr."/".$month;
		$n = rand(0,100000);
		$current_date=date("Y-m-d H:i:s");
		$id=Input::get('id');
		$image=Input::file('image');
		$invoice=Input::file('invoice');
		$qrcode=Input::file('qrcode');
		$created_by='';
		$modified_by='';

		if(isset($id))
		{
			$id=Input::get('id');
			$product=product::find($id);
			$product->updated_at = $current_date;

		}
		else
		{
			$product = new product();
			$product->created_at = $current_date;
		}

		if(Input::hasfile('image'))
		{
			if(isset($id))
			{          
				$old_img_name = $product->image;
				$old_destinationPath = $product->image_path;
				$image_old_path_name = $old_destinationPath."/".$old_img_name;

				if($old_img_name)
				{
					unlink($image_old_path_name);
				}
			}
			$extension = $image->getClientOriginalExtension();
			$image_name = 'Product_image'.$n.'.'.$extension;
			$product->image=$image_name;
			$product->image_path=$image_path;

			$image->move($image_path, $image_name);
		}
		elseif (Input::has('image')) {
			$image_update = Input::get('image');
			if($image_update=='')
			{
				$product->image='';
				$product->image_path='';
			}
		}
		if(Input::hasfile('invoice'))
		{
			if(isset($id))
			{          
				$old_invoice_name = $product->invoice;
				$old_destinationPath = $product->invoice_path;
				$invoice_old_path_name = $old_destinationPath."/".$old_invoice_name;

				if($old_invoice_name)
				{
					unlink($invoice_old_path_name);
				}
			}
			$extension = $invoice->getClientOriginalExtension();
			$invoice_name = 'invoice'.$n.'.'.$extension;
			$product->invoice=$invoice_name;
			$product->invoice_path=$invoice_path;

			$invoice->move($invoice_path, $invoice_name);
		}
		elseif (Input::has('invoice')) {
			$invoice_image_update = Input::get('invoice');
			if($invoice_image_update=='')
			{
				$product->invoice='';
				$product->invoice_path='';
			}
		}

		if(Input::hasfile('qrcode'))
		{
			if(isset($id))
			{          
				$old_qrcode_name = $product->qrcode_path;
				$old_destinationPath = $product->qrcode_path;
				$qrcode_old_path_name = $old_destinationPath."/".$old_qrcode_name;

				if($old_qrcode_name)
				{
					unlink($qrcode_old_path_name);
				}
			}
			$extension = $qrcode->getClientOriginalExtension();
			$qrcode_name = 'qrcode'.$n.'.'.$extension;
			$product->qrcode=$qrcode_name;
			$product->qrcode_path=$qrcode_path;

			$qrcode->move($qrcode_path, $qrcode_name);
		}
		elseif (Input::has('qrcode')) {
			$qrcode_image_update = Input::get('qrcode');
			if($qrcode_image_update=='')
			{
				$product->qrcode='';
				$product->qrcode_path='';
			}
		}

		$product->productcat_id = Input::get('productcat_id');
		$product->subcategory_id  = Input::get('subcategory_id');
		$product->brand_id  = Input::get('brand_id');
		$product->vendor_id  = Input::get('vendor_id');
		$product->color  = Input::get('color');
		$current_date=date("Y-m-d H:i:s");
		// $product->dateofpurchase  = $current_date;
		$product->dateofpurchase  = Input::get('dateofpurchase');
		$product->actualcost  = Input::get('actualcost');
		$product->rentalcost  = Input::get('rentalcost');
		$product->installationcost  = Input::get('installationcost');
		$product->machineserialnumber  = Input::get('machineserialnumber');
		$product->producttype  = Input::get('producttype');	
		$product->status  = 0;	
		$product->created_by  = $created_by;	
		$product->modified_by  = $modified_by;	

		$product->save(); 
		if(isset($product->id))
		{
			$result = array('status'=>'success','message'=>'Product saved successfully');
		}
		else
		{
			$result = array('status'=>'error','message'=>'OOPS!! Product not saved.');
		}
	}

	public function view_product()
	{
		$product = product::join('productcategory','productcategory.id','=','product.productcat_id')
		->join('subcategory','subcategory.id','=','product.subcategory_id')
		->join('brand','brand.id','=','product.brand_id')
		->join('vendor','vendor.id','=','product.vendor_id')
		->select('productcategory.productcategory','subcategory.subcategory','brand.brand','vendor.vendorname','product.*')
		->get();
		return json_encode($product);
	}

	public function edit_product()
	{
		$id =Input::get('id');
		$data = product::find($id);
		if(isset($data))
		{
			$result = array('status'=>'success','message'=>'Edit data successfully','data'=>$data);
		}
		else
		{
			$result = array('status'=>'error','message'=>'no record found');
		}
		return  json_encode($result);
	}

	public function table_status_change()
	{
		$id=Input::get('id');
		$table_name=Input::get('table_name');
		$status=Input::get('status');
		$update_result = DB::table($table_name)->where('id',$id)->update(['status' => $status]);
		if($update_result)
		{
			$res = array('status' => 'success','message' => 'status changed','data'=>$status);
		}
		else
		{
			$res = array('status' => 'error','message' => 'status not changed');
		}
		echo json_encode($res);
	}

	public function view_productdetails()
	{
		$product['productcategory']=productcategory::where('status','=',1)->get();
		$product['subcategory']=subcategory::where('status','=',1)->get();
		$product['vendor']=vendor::where('status','=',1)->get();
		$product['brand']=brand::where('status','=',1)->get();
		return json_encode($product);
	}

	public function master_menu()
	{
		$menu = menu::where('id','=',17)
		->where('parent_id','=',0)
		->get();

		if(isset($menu))
		{
			$response = array('status' => 'success','message'=>'menu successfully','data' => $menu );
		}
		else
		{
			$response = array('status' => 'error','message'=>'menu  failure' );
		}
		return json_encode($response);
	}

	public function master_submenu()
	{
		$sub_menu = menu::where('parent_id','=',17)->get();
		if(isset($sub_menu))
		{
			$response = array('status' => 'success','message'=>'menu successfully','data' => $sub_menu );
		}
		else
		{
			$response = array('status' => 'error','message'=>'menu  failure' );
		}
		return json_encode($response);
	}
// ----------------------------------START CUSTOMER ----------------------------------//
	public function add_customer()
	{	
		// $yr=date('Y');
		// $month=date('m');
		// $image_path = "Uploads/Customerphoto/".$yr."/".$month;
		// $n = rand(0,100000);
		// $current_date=date("Y-m-d H:i:s");
		$created_by='';
		$modified_by='';

		// if(isset($id))
		// {
		// 	$id=Input::get('id');
		// 	$customer=Customer::find($id);
		// 	$customer->updated_at = $current_date;
		// }

		// else
		// {
		// 	$customer = new Customer();
		// 	$customer->created_at = $current_date;
		// }

		// if(Input::hasfile('customerphoto'))
		// {
		// 	if(isset($id))
		// 	{          
		// 		$old_img_name = $customer->customerphoto;
		// 		$old_destinationPath = $customer->customerphoto_path;
		// 		$customer_old_path_name = $old_destinationPath."/".$old_img_name;

		// 		if($old_img_name)
		// 		{
		// 			unlink($customer_old_path_name);
		// 		}
		// 	}
		// 	$extension = $customerphoto->getClientOriginalExtension();
		// 	$image_name = 'customer_image'.$n.'.'.$extension;
		// 	$customer->customerphoto=$image_name;
		// 	$customer->customerphoto_path=$image_path;

		// 	$image->move($image_path, $image_name);
		// }

		// elseif (Input::has('customerphoto')) {
		// 	$customer_image_update = Input::get('customerphoto');
		// 	if($customer_image_update=='')
		// 	{
		// 		$customer->customerphoto='';
		// 		$customer->customerphoto_path='';
		// 	}
		// }

		$customer = new Customer();

		$customer->enquiry_id=Input::get('enquiry_id');
		$customer->name=Input::get('name');
		$customer->dob=Input::get('dob');
		$customer->gender=Input::get('gender');
		$customer->mobile1=Input::get('mobile1');
		$customer->mobile2=Input::get('mobile2');
		$customer->email1=Input::get('email1');
		$customer->email2=Input::get('email2');
		$customer->nativity=Input::get('nativity');
		$customer->marital_status=Input::get('marital_status');
		$customer->mother_tongue=Input::get('mother_tongue');
		$customer->rent_or_own=Input::get('rent_or_own');
		$customer->door_flatnum=Input::get('door_flatnum');
		$customer->floor_num=Input::get('floor_num');
		$customer->street=Input::get('street');
		$customer->area=Input::get('area');
		$customer->landmark=Input::get('landmark');
		$customer->district=Input::get('district');
		$customer->state=Input::get('state');
		$customer->pincode=Input::get('pincode');
		$customer->p_door_flatnum=Input::get('p_door_flatnum');
		$customer->p_floor_num=Input::get('p_floor_num');
		$customer->p_street=Input::get('p_street');
		$customer->p_area=Input::get('p_area');
		$customer->p_landmark=Input::get('p_landmark');
		$customer->p_district=Input::get('p_district');
		$customer->p_state=Input::get('p_state');
		$customer->p_pincode=Input::get('p_pincode');
		$reference_encoded = json_encode(Input::get('reference'));
		$customer->reference=$reference_encoded;
		$customer->companyname=Input::get('companyname');
		$customer->desgination=Input::get('desgination');
		$customer->department=Input::get('department');
		$customer->companyemail=Input::get('companyemail');
		$customer->companyphno=Input::get('companyphno');
		$customer->companyaddress=Input::get('companyaddress');
		$customer->id_proof=Input::get('id_proof');
		$customer->bank_name=Input::get('bank_name');
		$customer->bank_branch=Input::get('bank_branch');
		$customer->ifsc_code=Input::get('ifsc_code');
		$customer->account_num=Input::get('account_num');
		// $customer->upload_doument=Input::get('upload_doument');
		// $customer->upload_photo=Input::get('upload_photo');
		$customer->remarks=Input::get('remarks');
		$customer->attended=Input::get('attended');

		$customer->customerphoto='';
		$customer->customerphoto_path='';

		$customer->status  = 0;	
		$customer->created_by  = $created_by;	
		$customer->modified_by  = $modified_by;	

		$customer->save();

		if(isset($customer->id))
		{
			$result = array('status'=>'success','message'=>'Customer saved successfully');
		}
		else
		{
			$result = array('status'=>'error','message'=>'OOPS!! Customer not saved.');
		}

		// upload_document
		// upload_photo
		// customer_photopath
		// customer_photo
		// id_proof

	}
// ----------------------------------END CUSTOMER ----------------------------------//
}