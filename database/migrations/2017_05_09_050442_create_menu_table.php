<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('menu', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id');
            $table->string('menu_name');
            $table->string('icon_img');
            $table->integer('serial_no');
            $table->string('slug');
            $table->string('url');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('menu');
    }
}
