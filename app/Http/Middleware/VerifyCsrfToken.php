<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
    'userlist','storeuser','userlogin','enquiry','enquiryedit','apitemplate/get_templates','apitemplate/get_template_images',
    'apitemplate/template_info','adminrole','menulist','submenulist','masterdata_productcategory',
    'masterdata_subcategory','masterdata_brand','masterdata_vendor','masterdata_area','masterdata_nativity',
    'masterdata_baselocation','masterdata_mothertongue',
    //product API
    'add_product','edit_product','update_product','view_productdetails','view_area','masterdata_area',
    'master_menu','master_submenu','view_productcategory','view_subcategory','view_vendor','view_brand','view_nativity',
    'view_baselocation','view_mothertongue','brand_statuschange','nativity_statuschange',
    'areaedit','nativityedit','brandedit','vendoredit','subcategoryedit','productcategoryedit','mothertonguedit','baselocationedit',
    'sub_category_list','masterdata_addressproof','view_addressproof','addressproofedit','photoproofedit','masterdata_photoproof',
    'view_photoproof',
    //customer
    'add_customer',
    //common
    'table_status_change',
    ];
  }
