// Google-Maps
function myMap() {
    var maps1 = new google.maps.LatLng(13.0368, 80.2676)
    var mapOptions1 = {
        center: maps1,
        zoom: 15,
        draggable: false,
        scrollwheel: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var map1 = new google.maps.Map(document.getElementById("googleMap1"), mapOptions1);
    var marker = new google.maps.Marker({
        position: maps1
    });
    marker.setMap(map1);

    var infowindow = new google.maps.InfoWindow({
        content:"Acerental"
    });
    infowindow.open(map1, marker);
}


(function($) {

    $.fn.parallax = function(options) {

        var windowHeight = $(window).height();

        // Establish default settings
        var settings = $.extend({
            speed        : 0.15
        }, options);

        // Iterate over each object in collection
        return this.each( function() {

        	// Save a reference to the element
        	var $this = $(this);

        	// Set up Scroll Handler
        	$(document).scroll(function(){

    		        var scrollTop = $(window).scrollTop();
            	        var offset = $this.offset().top;
            	        var height = $this.outerHeight();

    		// Check if above or below viewport
			if (offset + height <= scrollTop || offset >= scrollTop + windowHeight) {
				return;
			}

			var yBgPosition = Math.round((offset - scrollTop) * settings.speed);

                 // Apply the Y Background Position to Set the Parallax Effect
    			$this.css('background-position', 'center ' + yBgPosition + 'px');

        	});
        });
    }
}(jQuery));

$('.pic1,.pic3,.pic2,.pic4').parallax({
	speed :	0.25
});

$('.pic5,.pic6').parallax({
	speed :	0.25
});

// media query event handler
if (matchMedia) {
    var mq = window.matchMedia("(min-width: 700px)");
    mq.addListener(WidthChange);
    WidthChange(mq);
}

// media query change
function WidthChange(mq) {
    if (mq.matches) {

        $('ul.nav li.dropdown').hover(function() {
            $(this).children('.dropdown-menu').stop(true, false, true).fadeToggle(500);
        });
    } else {
        $('ul.nav li.dropdown > a').click(function() {
            $(this).parent().siblings().find('.dropdown-menu').slideUp(300);
            $(this).next('.dropdown-menu').stop(true, false, true).slideToggle(300);
            return false;
        });
    }

}


$('#owl-demo').owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    navText: [
        "<i class=' fa fa-chevron-left fa-2x' style='margin-left:-1px'></i>",
        "<i class=' fa fa-chevron-right fa-2x' style='margin-right:-1px'></i>"
    ],
    dots: false,
    autoplay: true,
    animateIn: 'slideInUp',
    animateOut: 'slideOutUp',
    autoplayHoverPause: false,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 1
        },
        1000: {
            items: 1
        }
    }
})

$('#owl-demo2').owlCarousel({
    loop: true,
    nav: false,
    dots: false,
    autoplay: true,
    animateIn: 'slideInUp',
    animateOut: 'slideOutUp',
    autoplayHoverPause: false,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 3
        },
        1000: {
            items: 5
        }
    }
})
$('#owl-demo3').owlCarousel({
    loop: true,
    nav: false,
    dots: false,
    autoplay: true,
    animateIn: 'zoomInDown',
    animateOut: 'zoomOutDown',
    autoplayHoverPause: false,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 1
        },
        1000: {
            items: 1
        }
    }
})

$('#owl-demo11,#owl-demo12,#owl-demo13,#owl-demo14').owlCarousel({
    loop: true,
    nav:false,
    margin:10,
    navText: [
        "<i class=' fa fa-chevron-left fa-1x' style='margin-left:-1px'></i>",
        "<i class=' fa fa-chevron-right fa-1x' style='margin-right:-1px'></i>"
    ],
    dots: false,
    autoplay: true,
    animateIn: 'fadeIn',
    animateOut: 'fadeOut',
    autoplayHoverPause: false,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 2
        },
        1000: {
            items: 4
        }
    }
})
$(function(){

	var $window = $(window);		//Window object

	var scrollTime = 1.0;			//Scroll time
	var scrollDistance = 370;		//Distance. Use smaller value for shorter scroll and greater value for longer scroll

	$window.on("mousewheel DOMMouseScroll", function(event){

		event.preventDefault();

		var delta = event.originalEvent.wheelDelta/120 || -event.originalEvent.detail/3;
		var scrollTop = $window.scrollTop();
		var finalScroll = scrollTop - parseInt(delta*scrollDistance);

		TweenMax.to($window, scrollTime, {
			scrollTo : { y: finalScroll, autoKill:true },
				ease: Power1.linear,	//For more easing functions see http://api.greensock.com/js/com/greensock/easing/package-detail.html
				autoKill: true,
				overwrite: 5
			});

	});

});


  // Add smooth scrolling on all links inside the navbar
  $("#example-navbar-collapse-1 a, #arrow-btn a").on('click', function(event) {
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    }  // End if
  });

/*scroll-top*/

  $(window).scroll(function() {
      if ($(this).scrollTop() > 50) {
          $('#user-comment:hidden').stop(true, true).fadeIn();
      } else {
          $('#user-comment').stop(true, true).fadeOut();
      }
  });
  $('#scroll-top1').click(function() {
      $("html, body").animate({
          scrollTop: 0
      }, 2000);
      return false;
  });
