<?php

use Illuminate\Database\Seeder;

class BrandTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('brand')->delete();
     $brand = array(
  array('id' => '1','brand' => 'Samsung','status' => '1','created_by' => '','modified_by' => '','created_at' => '2017-06-24 11:45:34','updated_at' => '2017-06-24 11:45:34'),
  array('id' => '2','brand' => 'Voltas','status' => '1','created_by' => '','modified_by' => '','created_at' => '2017-07-07 07:34:14','updated_at' => '2017-07-07 07:34:14'),
  array('id' => '3','brand' => 'Blue Star','status' => '1','created_by' => '','modified_by' => '','created_at' => '2017-07-07 07:35:17','updated_at' => '2017-07-07 07:35:17'),
  array('id' => '4','brand' => 'Hitachi','status' => '1','created_by' => '','modified_by' => '','created_at' => '2017-07-07 07:35:40','updated_at' => '2017-07-07 07:35:40'),
  array('id' => '5','brand' => 'LG','status' => '1','created_by' => '','modified_by' => '','created_at' => '2017-07-07 07:35:56','updated_at' => '2017-07-07 07:35:56'),
  array('id' => '6','brand' => 'Panasonic','status' => '1','created_by' => '','modified_by' => '','created_at' => '2017-07-07 07:36:33','updated_at' => '2017-07-07 07:36:33'),
  array('id' => '7','brand' => 'Sony','status' => '1','created_by' => '','modified_by' => '','created_at' => '2017-07-07 07:36:58','updated_at' => '2017-07-07 07:36:58')
);
DB::table('brand')->insert($brand);	
    }
}
