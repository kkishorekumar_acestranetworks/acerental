@extends('websitemaster')
<div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header">
                        <h1>FAQ
   <small>Frequently Asked Questions</small>
 </h1> </div>
                </div>
                <div id="faq" class="col-md-9">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
         <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
           href="#collapse-1">
        What is the minimum rental period?
         </a>
       </h4> </div>
                            <div id="collapse-1" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <p>Acerental products can be rented for a minimum period of 3 months. </p>
                                </div>
                                <div class="panel-footer">
                                    <div class="btn-group btn-group-xs"><span class="btn">Was this question useful?</span><a class="btn btn-success" href="#"><i class="fa fa-thumbs-up"></i> Yes</a> <a class="btn btn-danger" href="#"><i class="fa fa-thumbs-down"></i> No</a></div>
                                    <div class="btn-group btn-group-xs pull-right hidden-xs"><a class="btn btn-primary" href="#">Report this question</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
         <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
           href="#collapse-2">
         Is there an agreement?
         </a>
       </h4> </div>
                            <div id="collapse-2" class="panel-collapse collapse">
                                <div class="panel-body"> Yes, you need to sign the terms and conditions provided along with the application form. </div>
                                <div class="panel-footer">
                                    <div class="btn-group btn-group-xs"><span class="btn">Was this question useful?</span><a class="btn btn-success" href="#"><i class="fa fa-thumbs-up"></i> Yes</a> <a class="btn btn-danger" href="#"><i class="fa fa-thumbs-down"></i> No</a></div>
                                    <div class="btn-group btn-group-xs pull-right hidden-xs"><a class="btn btn-primary" href="#">Report this question</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
         <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
           href="#collapse-3">
         How can I terminate the rental agreement?
         </a>
       </h4> </div>
                            <div id="collapse-3" class="panel-collapse collapse">
                                <div class="panel-body"> You may choose to discontinue the service after a period of 3 months. Acerental requests its client to provide a minimum of 15 days prior notice to facilitate a hassle free settlement. </div>
                                <div class="panel-footer">
                                    <div class="btn-group btn-group-xs"><span class="btn">Was this question useful?</span><a class="btn btn-success" href="#"><i class="fa fa-thumbs-up"></i> Yes</a> <a class="btn btn-danger" href="#"><i class="fa fa-thumbs-down"></i> No</a></div>
                                    <div class="btn-group btn-group-xs pull-right hidden-xs"><a class="btn btn-primary" href="#">Report this question</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
         <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
           href="#collapse-4">
        How much rent do I need to pay and when?
         </a>
       </h4> </div>
                            <div id="collapse-4" class="panel-collapse collapse">
                                <div class="panel-body"> Clients are encouraged to pay rent for the products on or before 7th of every month. Kindly refer to the product categories listed along with rent details in the website. Rent will be communicated without any ambiguity at the time of delivery and it will be documented in the application form. </div>
                                <div class="panel-footer">
                                    <div class="btn-group btn-group-xs"><span class="btn">Was this question useful?</span><a class="btn btn-success" href="#"><i class="fa fa-thumbs-up"></i> Yes</a> <a class="btn btn-danger" href="#"><i class="fa fa-thumbs-down"></i> No</a></div>
                                    <div class="btn-group btn-group-xs pull-right hidden-xs"><a class="btn btn-primary" href="#">Report this question</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
         <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
           href="#collapse-5">
        How much should I pay as advance?
         </a>
       </h4> </div>
                            <div id="collapse-5" class="panel-collapse collapse">
                                <div class="panel-body"> Clients will pay a refundable deposit which is equal to 3 months rent of the product chosen (varies according to each product) and Onetime non refundable processing charges of Rs. 500/- per product. </div>
                                <div class="panel-footer">
                                    <div class="btn-group btn-group-xs"><span class="btn">Was this question useful?</span><a class="btn btn-success" href="#"><i class="fa fa-thumbs-up"></i> Yes</a> <a class="btn btn-danger" href="#"><i class="fa fa-thumbs-down"></i> No</a></div>
                                    <div class="btn-group btn-group-xs pull-right hidden-xs"><a class="btn btn-primary" href="#">Report this question</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
         <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
           href="#collapse-6">
         Do I get rewards for referring a friend to Acerental?
         </a>
       </h4> </div>
                            <div id="collapse-6" class="panel-collapse collapse">
                                <div class="panel-body"> You could earn an attractive gift voucher worth Rs. 250/- for every person you refer to us. Once your friend’s application is successful and he/she enrolls as a Acerental customer, we will send the gift voucher in person or through courier. Referrals can be made through phone or email or by filling up your details and that of your friend in our contact us page. </div>
                                <div class="panel-footer">
                                    <div class="btn-group btn-group-xs"><span class="btn">Was this question useful?</span><a class="btn btn-success" href="#"><i class="fa fa-thumbs-up"></i> Yes</a> <a class="btn btn-danger" href="#"><i class="fa fa-thumbs-down"></i> No</a></div>
                                    <div class="btn-group btn-group-xs pull-right hidden-xs"><a class="btn btn-primary" href="#">Report this question</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
         <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
           href="#collapse-7">
         Are there any other charges apart from rent, processing charges and refundable deposit?
         </a>
       </h4> </div>
                            <div id="collapse-7" class="panel-collapse collapse">
                                <div class="panel-body"> No. </div>
                                <div class="panel-footer">
                                    <div class="btn-group btn-group-xs"><span class="btn">Was this question useful?</span><a class="btn btn-success" href="#"><i class="fa fa-thumbs-up"></i> Yes</a> <a class="btn btn-danger" href="#"><i class="fa fa-thumbs-down"></i> No</a></div>
                                    <div class="btn-group btn-group-xs pull-right hidden-xs"><a class="btn btn-primary" href="#">Report this question</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
         <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
           href="#collapse-8">
         When will I get my refundable deposit?
         </a>
       </h4> </div>
                            <div id="collapse-8" class="panel-collapse collapse">
                                <div class="panel-body"> Refundable deposit will be returned to you after the end of contact period or return of the product as the case may be. The returned products are subjected to quality check either at your place or at our office depending on the availability of our engineers. Upon clearance from quality team the rental deposit will be transferred to your account with one week. Any cost arising out of damages other than normal wear and tear will be deducted from the deposit. </div>
                                <div class="panel-footer">
                                    <div class="btn-group btn-group-xs"><span class="btn">Was this question useful?</span><a class="btn btn-success" href="#"><i class="fa fa-thumbs-up"></i> Yes</a> <a class="btn btn-danger" href="#"><i class="fa fa-thumbs-down"></i> No</a></div>
                                    <div class="btn-group btn-group-xs pull-right hidden-xs"><a class="btn btn-primary" href="#">Report this question</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
         <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
           href="#collapse-9">
         How can I pay the rent?
         </a>
       </h4> </div>
                            <div id="collapse-9" class="panel-collapse collapse">
                                <div class="panel-body"> Our collection executives will visit your place on or before 7th of every month to collect cash or cheque at your convenience. </div>
                                <div class="panel-footer">
                                    <div class="btn-group btn-group-xs"><span class="btn">Was this question useful?</span><a class="btn btn-success" href="#"><i class="fa fa-thumbs-up"></i> Yes</a> <a class="btn btn-danger" href="#"><i class="fa fa-thumbs-down"></i> No</a></div>
                                    <div class="btn-group btn-group-xs pull-right hidden-xs"><a class="btn btn-primary" href="#">Report this question</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
         <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
           href="#collapse-10">
         When can I expect the delivery of the product?
         </a>
       </h4> </div>
                            <div id="collapse-10" class="panel-collapse collapse">
                                <div class="panel-body"> Once you submit the application along with required information and supporting documents, speedy credit verification and approval process is initiated. Our average delivery timeline is 3-5 business days from the date of application. </div>
                                <div class="panel-footer">
                                    <div class="btn-group btn-group-xs"><span class="btn">Was this question useful?</span><a class="btn btn-success" href="#"><i class="fa fa-thumbs-up"></i> Yes</a> <a class="btn btn-danger" href="#"><i class="fa fa-thumbs-down"></i> No</a></div>
                                    <div class="btn-group btn-group-xs pull-right hidden-xs"><a class="btn btn-primary" href="#">Report this question</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
         <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
           href="#collapse-11">
         Who will take care of the installation?
         </a>
       </h4> </div>
                            <div id="collapse-11" class="panel-collapse collapse">
                                <div class="panel-body"> Acerental team will take care of standard installation. Any additional effort (no lift) towards delivery and customization on installation will be at additional cost to be borne by the client. Please speak to the executive while submitting the application for more details. </div>
                                <div class="panel-footer">
                                    <div class="btn-group btn-group-xs"><span class="btn">Was this question useful?</span><a class="btn btn-success" href="#"><i class="fa fa-thumbs-up"></i> Yes</a> <a class="btn btn-danger" href="#"><i class="fa fa-thumbs-down"></i> No</a></div>
                                    <div class="btn-group btn-group-xs pull-right hidden-xs"><a class="btn btn-primary" href="#">Report this question</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
         <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
           href="#collapse-12">
         What are the documentation requirements?
         </a>
       </h4> </div>
                            <div id="collapse-12" class="panel-collapse collapse">
                                <div class="panel-body"> Acerental requests clients to submit copy of standard KYC documents covering ID proof, address proof (permanent and current address) and company ID card along with duly completed application form. We request you to show the original document to the executive while signing the application which enables speedy verification and approval. </div>
                                <div class="panel-footer">
                                    <div class="btn-group btn-group-xs"><span class="btn">Was this question useful?</span><a class="btn btn-success" href="#"><i class="fa fa-thumbs-up"></i> Yes</a> <a class="btn btn-danger" href="#"><i class="fa fa-thumbs-down"></i> No</a></div>
                                    <div class="btn-group btn-group-xs pull-right hidden-xs"><a class="btn btn-primary" href="#">Report this question</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
         <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
           href="#collapse-13">
         Can I continue the service when I relocate?
         </a>
       </h4> </div>
                            <div id="collapse-13" class="panel-collapse collapse">
                                <div class="panel-body"> Acerental currently operates in Chennai. Relocation of products is to be handled only by Acerental team. Clients are advised to inform us 15 days in advance to help you in relocation. Kindly note appropriate documentation requirements and relocation charges will apply. </div>
                                <div class="panel-footer">
                                    <div class="btn-group btn-group-xs"><span class="btn">Was this question useful?</span><a class="btn btn-success" href="#"><i class="fa fa-thumbs-up"></i> Yes</a> <a class="btn btn-danger" href="#"><i class="fa fa-thumbs-down"></i> No</a></div>
                                    <div class="btn-group btn-group-xs pull-right hidden-xs"><a class="btn btn-primary" href="#">Report this question</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
         <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
           href="#collapse-14">
         Can I rent your products for a short period?
         </a>
       </h4> </div>
                            <div id="collapse-14" class="panel-collapse collapse">
                                <div class="panel-body"> The minimum rental period of products is 3 months. On a standardized basis, you may choose to terminate the agreement during this period. In such an event, Acerental would retain the initial deposit. Pro-rata rent for the month and onetime processing charges as applicable would be charged. You may choose to call us for any specific requirements. We will be happy to explore other possibilities. </div>
                                <div class="panel-footer">
                                    <div class="btn-group btn-group-xs"><span class="btn">Was this question useful?</span><a class="btn btn-success" href="#"><i class="fa fa-thumbs-up"></i> Yes</a> <a class="btn btn-danger" href="#"><i class="fa fa-thumbs-down"></i> No</a></div>
                                    <div class="btn-group btn-group-xs pull-right hidden-xs"><a class="btn btn-primary" href="#">Report this question</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
         <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
           href="#collapse-15">
         Is there a penalty applicable for any damage?
         </a>
       </h4> </div>
                            <div id="collapse-15" class="panel-collapse collapse">
                                <div class="panel-body"> In the event of an unintentional damage to any of our products, please do call us and lodge a service request with details of damage. We advise you to not engage any other service engineer which is considered as breach of the agreement. Cost of repair / damage to be borne by the client at actuals. Cost of product will be recovered in the event of any irreparable damages or at the sole discretion of Acerental. We take pictures of the products upon installation which mutually helps at the time of product return. </div>
                                <div class="panel-footer">
                                    <div class="btn-group btn-group-xs"><span class="btn">Was this question useful?</span><a class="btn btn-success" href="#"><i class="fa fa-thumbs-up"></i> Yes</a> <a class="btn btn-danger" href="#"><i class="fa fa-thumbs-down"></i> No</a></div>
                                    <div class="btn-group btn-group-xs pull-right hidden-xs"><a class="btn btn-primary" href="#">Report this question</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
         <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
           href="#collapse-16">
         Can I buy the product from Acerental during the rental period?
         </a>
       </h4> </div>
                            <div id="collapse-16" class="panel-collapse collapse">
                                <div class="panel-body"> Yes. As a distinct advantage with Acerental, you can choose to own the product after a minimum rental period of 3 months. Exercising the option to buy provides you continuity of product and at a price that is reasonable and attractive. As an additional benefit, we would continue to provide service support for a period of 6 months from the time you buy the product. </div>
                                <div class="panel-footer">
                                    <div class="btn-group btn-group-xs"><span class="btn">Was this question useful?</span><a class="btn btn-success" href="#"><i class="fa fa-thumbs-up"></i> Yes</a> <a class="btn btn-danger" href="#"><i class="fa fa-thumbs-down"></i> No</a></div>
                                    <div class="btn-group btn-group-xs pull-right hidden-xs"><a class="btn btn-primary" href="#">Report this question</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
         <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
           href="#collapse-17">
      How do I register a complaint or feedback?
         </a>
       </h4> </div>
                            <div id="collapse-17" class="panel-collapse collapse">
                                <div class="panel-body"> Please call us at 044 3100 3040 (or) 044 3100 4050 from 10am to 7pm on all working days or email us at feedback@Acerental.com </div>
                                <div class="panel-footer">
                                    <div class="btn-group btn-group-xs"><span class="btn">Was this question useful?</span><a class="btn btn-success" href="#"><i class="fa fa-thumbs-up"></i> Yes</a> <a class="btn btn-danger" href="#"><i class="fa fa-thumbs-down"></i> No</a></div>
                                    <div class="btn-group btn-group-xs pull-right hidden-xs"><a class="btn btn-primary" href="#">Report this question</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
         <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
           href="#collapse-18">
        Can my friend or nominee take over my rental product?
         </a>
       </h4> </div>
                            <div id="collapse-18" class="panel-collapse collapse">
                                <div class="panel-body"> Yes, please inform us 15 days in advance to execute necessary documentation and activate the new client profile. </div>
                                <div class="panel-footer">
                                    <div class="btn-group btn-group-xs"><span class="btn">Was this question useful?</span><a class="btn btn-success" href="#"><i class="fa fa-thumbs-up"></i> Yes</a> <a class="btn btn-danger" href="#"><i class="fa fa-thumbs-down"></i> No</a></div>
                                    <div class="btn-group btn-group-xs pull-right hidden-xs"><a class="btn btn-primary" href="#">Report this question</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
         <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
           href="#collapse-19">
         What should I do in case of a service / maintenance request?
         </a>
       </h4> </div>
                            <div id="collapse-19" class="panel-collapse collapse">
                                <div class="panel-body"> Please feel free to reach us for a service request. Ongoing service / maintenance will be managed by us at no additional cost. Cost of spares and damage to be borne by the client at actuals. Depending on the lead time for service in case of a repair, we will attempt to provide a replacement until your product is ready. </div>
                                <div class="panel-footer">
                                    <div class="btn-group btn-group-xs"><span class="btn">Was this question useful?</span><a class="btn btn-success" href="#"><i class="fa fa-thumbs-up"></i> Yes</a> <a class="btn btn-danger" href="#"><i class="fa fa-thumbs-down"></i> No</a></div>
                                    <div class="btn-group btn-group-xs pull-right hidden-xs"><a class="btn btn-primary" href="#">Report this question</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
         <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
           href="#collapse-20">
         Are my personal details maintained with you safe?
         </a>
       </h4> </div>
                            <div id="collapse-20" class="panel-collapse collapse">
                                <div class="panel-body"> Yes, we deal with your details with utmost confidentiality. Your personal details will not be shared with anybody. </div>
                                <div class="panel-footer">
                                    <div class="btn-group btn-group-xs"><span class="btn">Was this question useful?</span><a class="btn btn-success" href="#"><i class="fa fa-thumbs-up"></i> Yes</a> <a class="btn btn-danger" href="#"><i class="fa fa-thumbs-down"></i> No</a></div>
                                    <div class="btn-group btn-group-xs pull-right hidden-xs"><a class="btn btn-primary" href="#">Report this question</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
         <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
           href="#collapse-21">
        What is my deliverable on maintenance of the product?
         </a>
       </h4> </div>
                            <div id="collapse-21" class="panel-collapse collapse">
                                <div class="panel-body"> We expect you to handle the products with care as it avoids issues and provides uninterrupted service. Please note our clients also choose to buy the same product during the rental period and hence a proper care helps. </div>
                                <div class="panel-footer">
                                    <div class="btn-group btn-group-xs"><span class="btn">Was this question useful?</span><a class="btn btn-success" href="#"><i class="fa fa-thumbs-up"></i> Yes</a> <a class="btn btn-danger" href="#"><i class="fa fa-thumbs-down"></i> No</a></div>
                                    <div class="btn-group btn-group-xs pull-right hidden-xs"><a class="btn btn-primary" href="#">Report this question</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
         <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
           href="#collapse-22">
         What is the average time to complete your approval process?
         </a>
       </h4> </div>
                            <div id="collapse-22" class="panel-collapse collapse">
                                <div class="panel-body"> We would endeavor to complete the approval process and deliver of the product within 3-5 working days upon receipt of your application complete in all respects. </div>
                                <div class="panel-footer">
                                    <div class="btn-group btn-group-xs"><span class="btn">Was this question useful?</span><a class="btn btn-success" href="#"><i class="fa fa-thumbs-up"></i> Yes</a> <a class="btn btn-danger" href="#"><i class="fa fa-thumbs-down"></i> No</a></div>
                                    <div class="btn-group btn-group-xs pull-right hidden-xs"><a class="btn btn-primary" href="#">Report this question</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
         <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
           href="#collapse-23">
        I want to rent more products, what should I do?
         </a>
       </h4> </div>
                            <div id="collapse-23" class="panel-collapse collapse">
                                <div class="panel-body"> We will be happy to hear your request. We will be able to deliver the products faster for existing clients. Kindly note onetime Processing charges of Rs. 500/- apply per product. </div>
                                <div class="panel-footer">
                                    <div class="btn-group btn-group-xs"><span class="btn">Was this question useful?</span><a class="btn btn-success" href="#"><i class="fa fa-thumbs-up"></i> Yes</a> <a class="btn btn-danger" href="#"><i class="fa fa-thumbs-down"></i> No</a></div>
                                    <div class="btn-group btn-group-xs pull-right hidden-xs"><a class="btn btn-primary" href="#">Report this question</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>