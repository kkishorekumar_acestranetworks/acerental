 <nav class="navbar navbar-default navbar-custom  navbar-sticky" role="navigation">
            <div class="container ">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle col2" data-toggle="collapse" data-target="#example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                    <a class="navbar-brand" href="#"><img id="myImage" class="img-responsive" src="img/acerental-logo.png" /> </a>
                </div>
                <div class="collapse navbar-collapse" id="example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown  active"><a href="#home">HOME</a> </li>
                        <li class="dropdown"><a href="#categories">CATEGORIES<span class="fa fa-angle-down icons-dropdown pad1"></span></a>
                            <ul class="dropdown-menu dropdown-menu-left col-xs-12">
                                <li><a href="Appliances.html">APPLIANCES</a></li>
                                <li><a href="Furniture.html">FURNITURE</a></li>
                                <li><a href="Automobile.html">AUTOMOBILE</a></li>
                                <li><a href="Electronics.html">ELECTRONICS</a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a href="#why_acerental">WHY ACERENTAL?</a> </li>
                        <li class="dropdown"><a href="#how_it_works">HOW IT WORKS</a> </li>
                        <li class="dropdown"><a href="#need_help">NEED HELP<span class="fa fa-angle-down icons-dropdown pad1"></span></a> </li>
                        <li class="dropdown"><a href="#"><i class="fa fa-search"></i></a>
                            <ul class="dropdown-menu pad5" style="display: none;">
                                <form role="search">
                                    <div class="input-group search-bar">
                                        <input class="form-control" placeholder="Search" name="q" type="text">
                                        <div class="input-group-btn">
                                            <button class="btn btn-default" type="submit"><i class="fa fa-search col3"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </ul>
                        </li>
                        <li class=""><a class="col3" href="#"><i class="fa fa-shopping-cart"></i></a> </li>
                        <li class=""><a class="col3" href="#"><i class="fa fa-user"></i></a> </li>
                    </ul>
                </div>
            </div>
        </nav>