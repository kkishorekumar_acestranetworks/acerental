<?php



/*Website Route*/
Route::get('/', 'Website\homeController@homepage');
Route::get('/aboutus','Website\homeController@aboutus');
Route::get('/contactus','Website\homeController@contactus');
Route::get('/privacypolicy','Website\homeController@privacypolicy');
Route::get('/faq','Website\homeController@faq');
Route::get('/termsofuse','Website\homeController@termsofuse');
Route::get('/categories','Website\homeController@categories');

/*Seprate Route for Acestra Webiste Template */
Route::any('/delete_image/{id}','templateController@delete_image');

Route::group(['prefix' => 'template'], function () {

Route::any('/template','templateController@template');
Route::any('/store','templateController@store');
Route::any('/edit/{id}','templateController@edit');
Route::any('/delete_image/{id}','templateController@delete_image');
Route::any('/delete_template/{id}','templateController@delete_template');

Route::any('/logo_store','templateController@logo_store');
Route::any('/logo','templateController@logo');
Route::any('/logo_edit/{id}','templateController@logo_edit');
Route::any('/delete_logo/{id}','templateController@delete_logo');
Route::any('/delete_logo_image/{id}','templateController@delete_logo_image');


});

Route::group(['prefix' => 'apitemplate'], function () {
Route::any('/get_templates','templateController@get_templates');
Route::any('/get_template_images/{id}','templateController@get_template_images');
Route::any('/template_info/{id}','templateController@template_info');
Route::any('/get_logo','templateController@get_logo');
});


Route::group(['prefix' => 'admin'], function () {
  Route::get('/login', 'AdminAuth\LoginController@showLoginForm');
  Route::post('/login', 'AdminAuth\LoginController@login');
  Route::post('/logout', 'AdminAuth\LoginController@logout');
  Route::get('/register', 'AdminAuth\RegisterController@showRegistrationForm');
  Route::post('/register', 'AdminAuth\RegisterController@register');
  Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail');
  Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset');
  Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm');
  Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');
});

Route::group(['prefix' => 'user'], function () {
  Route::get('/login', 'UserAuth\LoginController@showLoginForm');
  Route::post('/login', 'UserAuth\LoginController@login');
  Route::post('/logout', 'UserAuth\LoginController@logout');
  Route::get('/register', 'UserAuth\RegisterController@showRegistrationForm');
  Route::post('/register', 'UserAuth\RegisterController@register');
  Route::post('/password/email', 'UserAuth\ForgotPasswordController@sendResetLinkEmail');
  Route::post('/password/reset', 'UserAuth\ResetPasswordController@reset');
  Route::get('/password/reset', 'UserAuth\ForgotPasswordController@showLinkRequestForm');
  Route::get('/password/reset/{token}', 'UserAuth\ResetPasswordController@showResetForm');
});

/*Route for Acerental Dashboard for saravanan*/
  Route::any('storeuser','Api\apiController@storeuser');
  Route::post('userlogin','Api\apiController@userlogin');
  Route::get('userlist','Api\apiController@userlist');
  Route::any('menulist','Api\apiController@menulist');
  Route::any('submenulist','Api\apiController@submenulist');
  Route::any('master_menu','Api\apiController@master_menu');
  Route::any('master_submenu','Api\apiController@master_submenu');

  Route::any('adminrole','Api\apiController@adminrole'); /*Admin roles*/
  Route::any('rolelist','Api\apiController@rolelist');

  Route::any('enquiry','Api\apiController@enquiry');
  Route::get('enquirylist','Api\apiController@enquirylist');
  Route::any('enquiryedit','Api\apiController@enquiryedit');
  // Route::any('statuschange','Api\apiController@statuschange');
  Route::any('enquirydelete','Api\apiController@enquirydelete');

  Route::any('masterdata_productcategory','Api\apiController@masterdata_productcategory'); /*Master Data Adding*/
  Route::any('masterdata_subcategory','Api\apiController@masterdata_subcategory');
  Route::any('masterdata_vendor','Api\apiController@masterdata_vendor');
  Route::any('masterdata_brand','Api\apiController@masterdata_brand');
  Route::any('masterdata_nativity','Api\apiController@masterdata_nativity');
  Route::any('masterdata_area','Api\apiController@masterdata_area');
  Route::any('masterdata_baselocation','Api\apiController@masterdata_baselocation');
  Route::any('masterdata_mothertongue','Api\apiController@masterdata_mothertongue');

  
  Route::any('view_productcategory','Api\apiController@view_productcategory'); /*Master Data view*/
  Route::any('view_subcategory','Api\apiController@view_subcategory');
  Route::any('view_vendor','Api\apiController@view_vendor');
  Route::any('view_brand','Api\apiController@view_brand');
  Route::any('view_nativity','Api\apiController@view_nativity');
  Route::any('view_area','Api\apiController@view_area');
  Route::any('view_baselocation','Api\apiController@view_baselocation');
  Route::any('view_mothertongue','Api\apiController@view_mothertongue');

  Route::any('productcategoryedit','Api\apiController@productcategoryedit');
  Route::any('subcategoryedit','Api\apiController@subcategoryedit');
  Route::any('vendoredit','Api\apiController@vendoredit');
  Route::any('brandedit','Api\apiController@brandedit');
  Route::any('nativityedit','Api\apiController@nativityedit');
  Route::any('areaedit','Api\apiController@areaedit');
  Route::any('baselocationedit','Api\apiController@baselocationedit');
  Route::any('mothertonguedit','Api\apiController@mothertonguedit');
  Route::any('view_addressproof','Api\apiController@view_addressproof');
  Route::any('masterdata_addressproof','Api\apiController@masterdata_addressproof');
  Route::any('addressproofedit','Api\apiController@addressproofedit');
  Route::any('view_photoproof','Api\apiController@view_photoproof');
  Route::any('masterdata_photoproof','Api\apiController@masterdata_photoproof');
  Route::any('photoproofedit','Api\apiController@photoproofedit');

  Route::any('table_status_change','Api\apiController@table_status_change'); 
  // Route::any('vendor_statuschange','Api\apiController@vendor_statuschange'); /*Master Data status change*/
  // Route::any('nativity_statuschange','Api\apiController@nativity_statuschange');
  // Route::any('area_statuschange','Api\apiController@area_statuschange');
  // Route::any('brand_statuschange','Api\apiController@brand_statuschange');
  // Route::any('subcategory_statuschange','Api\apiController@subcategory_statuschange');
  // Route::any('productcategory_statuschange','Api\apiController@productcategory_statuschange');
  // Route::any('baselocation_statuschange','Api\apiController@baselocation_statuschange');
  // Route::any('mothertongue_statuschange','Api\apiController@mothertongue_statuschange');

  Route::any('view_productdetails','Api\apiController@view_productdetails'); /*To load data in product form*/
  Route::any('add_product','Api\apiController@add_product'); /*To add new product*/
  Route::any('view_product','Api\apiController@view_product'); /*To view all product*/
  Route::any('edit_product','Api\apiController@edit_product'); /*To edit a single product*/
  Route::any('update_product','Api\apiController@update_product'); /*To remove a product image*/
 
  Route::any('sub_category_list','Api\apiController@sub_category_list'); /*To add new product*/
 //customer
  Route::any('add_customer','Api\apiController@add_customer'); /*To add new product*/
  
  
 