<?php

use Illuminate\Database\Seeder;

class AreaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('area')->delete();
      $area = array(
		  array('id' => '1','areaname' => 'Mylapore','status' => '1','created_by' => '','modified_by' => '','created_at' => '2017-06-28 12:11:51','updated_at' => '2017-06-28 12:11:51'),
		  array('id' => '2','areaname' => 'Chennai','status' => '1','created_by' => '','modified_by' => '','created_at' => '2017-06-28 12:11:59','updated_at' => '2017-06-28 12:11:59')
		);
      DB::table('area')->insert($area);	
    }
}
