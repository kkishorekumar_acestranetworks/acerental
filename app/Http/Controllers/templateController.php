<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\template;
use App\Models\slider;
use App\Models\logo;
use Illuminate\Support\Facades\Input;
use File;


class templateController extends Controller
{
  public function home()
  {
      $list=template::all();

      return view('admin.home',$list);
  }

   public function store() /*for dashboard*/
{  
      $id=Input::get('id');
      $current_date=date("Y-m-d H:i:s");
      $mobile_image=input::file('mobile_image');

     if(isset($id) && $id != '')
      {
   
        $id=Input::get('id');
        $template=template::find($id);
        $template->updated_at = $current_date;

       }

       else
       {
        
        $template = new template();
        $template->created_at = $current_date;
       
       }

      $yr=date('Y');
      $month=date('m');
      $path = "Uploads/Template/".$yr."/".$month;
      $n = rand(0,100000);
      
     if(Input::hasfile('mobile_image'))
       {  
         if(isset($id))
         {          
                   $old_img_name = $template->mobile_image;
                   $old_destinationPath = $template->mobile_image_path;
                   $image_old_path_name = $old_destinationPath."/".$old_img_name;

                       if($old_img_name)
                  {
                      unlink($image_old_path_name);
                      
                  }
               
         }

          $extension = $mobile_image->getClientOriginalExtension();
          $n = rand(0,100000);
          $image_name = 'mobile_image'.$n.'.'.$extension;
          $template->mobile_image=$image_name;
          $template->mobile_image_path=$path;

          $mobile_image->move($path, $image_name);

      }  

 
     $template->name=input::get('name');
     $template->subname=Input::get('subname');
     $template->url=Input::get('url');
     $template->category=Input::get('category');
     $template->description=Input::get('description');
     $template->status=1;
     
    $template->save();

  $id= $template->id;



 if(isset($id))
   {  

      $image=Input::file('image');
      
      if(Input::hasfile('image'))
      {
         foreach ($image as $images)
          {

          $extension = $images->getClientOriginalExtension();
          $n = rand(0,100000);
          $filename = 'template_'.$n.'.'.$extension;
          $img = new slider();
          $img->image=$filename;
          $img->image_path=$path;
          $img->p_id = $id;
          $images->move($path, $filename);
          $img->save();

          }
      }
     

      $response = array('status' => 'success','message'=>'data save successfuly');

      $list=template::paginate(10);

      return view('admin.home',compact('list'));


/* echo json_encode($response);*/
   }
 }
public function delete_image($id)
{    
    $image = slider::where('id','=',$id)
            ->first();
         
    if(isset($id))
         {        
                   $old_img_name = $image->image;
                   $old_destinationPath = $image->image_path;         
                   $image_old_path_name = $old_destinationPath."/".$old_img_name;


                   if($old_img_name)
                  {
                      $image->delete();
                      
                  }

             return redirect()->back();

               
         }
   

}
 public function template()
   {  
      $list=template::paginate(10);

      return view('admin.home',compact('list'));

   }
/*
public function template()
{ 

  $data['list'] = template::all();


  return view('admin.home',$data['list']);


}
*/
 public function edit($id)
   {

    $data = template::find($id);

    $images = slider::where('p_id','=',$id)->get();


    $list=template::paginate(10);

   return view('admin.home',compact('list','data','images'));

   }


 public function delete_template($id)
  {
        
         $template = template::find($id);

         $template->delete();

         $p_id = slider::where('p_id','=',$id)->get();

         $list=template::all();
        
        return view('admin.home',compact('list'));   
       
  }   



public function get_templates() /*for website*/
{

  $category = input::get('category');

 if($category == 'all')
  {
    $template = template::all();
   }
   else
   {
     $template= template::where('category',$category)->get();
    }


    $array=array();

       foreach ($template as $key => $value)
                    {
                      $slider_image=slider::where('p_id','=',$value->id)
                      ->first();

                      if(isset($slider_image))
                      {
                        $image_path=$slider_image->image_path;
                        $image=$slider_image->image;
                        $image_name=url('/').'/'.$image_path.'/'.$image;
                        $array []= (object) array(
                          'id'=>$value->id,
                          'name'=>$value->name,
                          'url'=>$value->url,
                          'mobile_image'=>$value->mobile_image,
                          'mobile_path'=>$value->mobile_image_path,
                          'category'=>$value->category,
                          'subname'=>$value->subname,
                          'image'=>$image_name);
                      }

                    }
                    if(isset($array) && count($array)>0)
                    {
                      $res = array('status'=>'success','message'=>'data list','list'=>$array);

                    }
                    else
                    {
                      $res = array('status'=>'error','message'=>'data save Failer');

                    }

                    echo json_encode($res);
                  }

 public function get_template_images($id)/*for website*/
  {

     $data= slider::where('p_id','=',$id)->get();



     if(isset($data))
          {
             $res = array('status'=>'success','message'=>'data list','list'=>$data);
          }
     else
        {
            $res = array('status'=>'error','message'=>'data save Failer');

        }


        echo json_encode($res);
  }


   public function template_info($id) /*for website*/

   {
     $data = template::join('slider','slider.p_id','=','template.id')
            ->where('template.id','=',$id)
            ->select('template.id','template.name','template.description','template.url','slider.image','slider.image_path','template.category')
            ->first();
          

    $images = slider::where('p_id','=',$id)
              ->get(); 

    $related=template::where('id','!=',$id)
               ->where('category','=',$data->category)
               ->take(3)
               ->get();

              $array=array();

       foreach ($related as $key => $value)
                    {
                      $slider_image=slider::where('p_id','=',$value->id)
                      ->first();

                      if(isset($slider_image))
                      {
                        $image_path=$slider_image->image_path;
                        $image=$slider_image->image;
                        $image_name=url('/').'/'.$image_path.'/'.$image;
                        $array []= (object) array(
                          'id'=>$value->id,
                          'name'=>$value->name,
                          'url'=>$value->url,
                          'mobile_image'=>$value->mobile_image,
                          'mobile_path'=>$value->mobile_image_path,
                          'category'=>$value->category,
                          'subname'=>$value->subname,
                          'image'=>$image_name);
                      }

                    }
                
           

if(isset($images))
          {
             $res = array('status'=>'success','message'=>'data list','list'=>$data,'slider'=>$images,'related'=>$array);
          }
        else
          {
            $res = array('status'=>'error','message'=>'data save Failer','list'=>$images);
          }


        echo json_encode($res);

   }

   public function logo()
   {  
      $logo_list=logo::paginate(10);

      return view('admin.logo',compact('logo_list'));

   }

   public function logo_store()
   {  
      
      $yr=date('Y');
      $month=date('m');
      $path = "Uploads/Logo/".$yr."/".$month;
      $n = rand(0,100000);
      $current_date=date("Y-m-d H:i:s");
      
      $id=Input::get('id');
      $image=Input::file('image');
  
        if(isset($id) && $id != '')
      {
        $id=Input::get('id');
        $logo=logo::find($id);
        $logo->updated_at = $current_date;

       }

       else
       {

        $logo = new logo();
        $logo->created_at = $current_date;
       
       }


          if(Input::hasfile('image'))
       {
         if(isset($id))
         {          
                   $old_img_name = $logo->image;
                   $old_destinationPath = $logo->image_path;
                   $image_old_path_name = $old_destinationPath."/".$old_img_name;

                   if($old_img_name)
                  {
                      unlink($image_old_path_name);
                      
                  }
               
         }
          $extension = $image->getClientOriginalExtension();
          $n = rand(0,100000);
          $image_name = 'logo_image'.$n.'.'.$extension;
          $logo->image=$image_name;
          $logo->image_path=$path;

          $image->move($path, $image_name);

      } 

        $logo->name = Input::get('name');
        $logo->rate  = Input::get('rate');
        $logo->slug = str_slug(Input::get('slug'), "_");
        $logo->status='1';

        
         $logo->save(); 

         $logo_list=logo::paginate(10);

        return view('admin.logo',compact('logo_list'));
    

 }

 public function logo_edit($id)
  {
      $data=logo::find($id);

     
      $logo_list=logo::paginate(10);


      return view('admin.logo',compact('data','logo_list'));

  }    
/* public function  delete_logo_image($id)
 {    
  
       $image = logo::where('id','=',$id)
            ->first();
           
    if(isset($id))
         {        
                   $old_img_name = $image->image;
                   $old_destinationPath = $image->image_path; 
                  
                   $image_old_path_name = $old_destinationPath."/".$old_img_name;

                   if($old_img_name)
                  {   
                       unlink($image_old_path_name);
                  }

             return redirect()->back();

               
         }
 }*/
  public function delete_logo($id)
  {
       
        $logo = logo::find($id);
        $logo->delete();

        $logo_list=logo::all();

        return view('admin.logo',compact('logo_list'));   
       
  } 

  public function get_logo()/*for website*/
  { 

      $logo_list=logo::all();

      if(isset($logo_list))
    {
      $response = array('status' => 'success','message'=>'data list','list' =>$logo_list );

    }

    else
     {

      $response = array('status' => 'error','message'=>'data save Failed' );

    }
      return json_encode($response);

  }
   
   }

  /* public function store() for dashboard
{  
      $id=Input::get('id');

      $current_date=date("Y-m-d H:i:s");

      $mobile_image=input::file('mobile_image');

     if(isset($id) && $id != '')
      {
   
        $id=Input::get('id');
       
        $template=template::find($id);

        $template->updated_at = $current_date;

       }

       else
       {
        
        $template = new template();

        $template->created_at = $current_date;
       
       }

      $yr=date('Y');
      $month=date('m');
      $path = "Uploads/Template/".$yr."/".$month;
      $n = rand(0,100000);
      
     if(Input::hasfile('mobile_image'))
       {  
         if(isset($id))
         {          
                   $old_img_name = $template->mobile_image;
                   $old_destinationPath = $template->mobile_image_path;
                   $image_old_path_name = $old_destinationPath."/".$old_img_name;

                       if($old_img_name)
                  {
                      unlink($image_old_path_name);
                      
                  }
               
         }

          $extension = $mobile_image->getClientOriginalExtension();
          $n = rand(0,100000);
          $image_name = 'mobile_image'.$n.'.'.$extension;
          $template->mobile_image=$image_name;
          $template->mobile_image_path=$path;

          $mobile_image->move($path, $image_name);

      }  

 
     $template->name=input::get('name');
     $template->subname=Input::get('subname');
     $template->url=Input::get('url');
     $template->category=Input::get('category');
     $template->description=Input::get('description');
     $template->status=1;
     
    $template->save();

  $id= $template->id;


 if(isset($id))
   {
      $image=Input::file('image');
      
      if(Input::hasfile('image'))
      {
         foreach ($image as $images)
          {

          $extension = $images->getClientOriginalExtension();
          $n = rand(0,100000);
          $filename = 'template_'.$n.'.'.$extension;
          $img = new slider();
          $img->image=$filename;
          $img->image_path=$path;
          $img->p_id = $id;
          $images->move($path, $filename);
          $img->save();

          }
      }
     

      $response = array('status' => 'success','message'=>'data save successfuly');

      $list=template::all();

      return view('admin.home',compact('list'));


 echo json_encode($response);
   }
 }
*/

  