<?php

use Illuminate\Database\Seeder;

class AddressproofTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('addressproof')->delete();
      $addressproof = array(
		  array('id' => '1','addressproof_name' => 'Voter Id','status' => '1','created_by' => '','modified_by' => '','created_at' => '2017-07-28 12:11:51','updated_at' => '2017-06-28 12:11:51'),

		  array('id' => '2','addressproof_name' => 'PAN Card','status' => '1','created_by' => '','modified_by' => '','created_at' => '2017-07-28 12:11:51','updated_at' => '2017-06-28 12:11:51'),

		  array('id' => '3','addressproof_name' => 'Passport','status' => '1','created_by' => '','modified_by' => '','created_at' => '2017-07-28 12:11:51','updated_at' => '2017-06-28 12:11:51'),

		  array('id' => '4','addressproof_name' => 'Driving License','status' => '1','created_by' => '','modified_by' => '','created_at' => '2017-07-28 12:11:51','updated_at' => '2017-06-28 12:11:51'),
		);
      DB::table('addressproof')->insert($addressproof);	
    }
}
