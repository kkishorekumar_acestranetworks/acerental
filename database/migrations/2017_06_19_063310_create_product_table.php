<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('product', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('productcat_id');
           /* $table->foreign('product_id')->references('id')->on('productcategory');*/
            $table->integer('subcategory_id');
          /*  $table->foreign('subcategory_id')->references('id')->on('subcategory');*/
            $table->integer('brand_id');
            /*$table->foreign('brand_id')->references('id')->on('brand');*/
            $table->integer('vendor_id');
            /*$table->foreign('vendor_id')->references('id')->on('vendor');*/
            $table->string('color');
            $table->date('dateofpurchase');
            $table->string('actualcost');
            $table->string('rentalcost');
            $table->string('installationcost');
            $table->string('machineserialnumber');
            $table->string('invoice_path');
            $table->string('invoice');
            $table->string('qrcode_path');
            $table->string('qrcode');
            $table->string('image_path');
            $table->string('image');
            $table->string('producttype');
            $table->string('created_by',50);
            $table->string('modified_by',50);
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product');
    }
}
