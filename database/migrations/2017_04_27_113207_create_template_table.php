<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemplateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('template', function(Blueprint $table) 
        {
            $table->increments('id');
            $table->string('name');
            $table->string('subname');
            $table->string('url');
            $table->string('slug_name');
            $table->string('category');
            $table->string('mobile_image');
            $table->string('mobile_image_path');
            $table->string('description');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('template');
    }
}
