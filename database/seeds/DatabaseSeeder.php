<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
         $this->call(AreaTableSeeder::class);
         $this->call(menuTableSeeder::class);
         $this->call(BaselocationTableSeeder::class);
         $this->call(MothertongeTableSeeder::class);
         $this->call(BrandTableSeeder::class);
         $this->call(ProductcategoryTableSeeder::class);
         $this->call(SubcategoryTableSeeder::class);
         $this->call(VendorTableSeeder::class);
         $this->call(RegisterTableSeeder::class);



    }
}
