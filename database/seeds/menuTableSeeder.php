<?php

use Illuminate\Database\Seeder;

class menuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      DB::table('menu')->delete();

       $menu = array(
  			array('id' => '1','parent_id' => '0','menu_name' => 'Dashboard','icon_img' => 'fa fa-tachometer','serial_no' => '0',
          'slug' => 'NULL','url' => 'home','status' => '1','created_at' => '2017-05-08 11:07:22','updated_at' => '2017-05-08 11:07:22'),

  			array('id' => '2','parent_id' => '0','menu_name' => 'Admin User','icon_img' => 'fa fa-user','serial_no' => '0','slug' => '',
          'url' => '','status' => '1','created_at' => '2017-05-08 11:07:22','updated_at' => '2017-05-08 11:07:22'),

        array('id' => '3','parent_id' => '2','menu_name' =>'Role','icon_img' => '','serial_no' => '0','slug' => '','url' => 'role',
          'status' => '1','created_at' => '2017-05-08 11:07:22','updated_at' => '2017-05-08 11:07:22'),

        array('id' => '4','parent_id' => '2','menu_name' => 'Managepermission','icon_img' => '','serial_no' => '0','slug' => '',
          'url' =>'managepermission','status' => '1','created_at' => '2017-05-08 11:07:22','updated_at' => '2017-05-08 11:07:22'),

        array('id' => '5','parent_id' => '2','menu_name' =>'Createlogin','icon_img' => '','serial_no' => '0','slug' => '',
          'url' => 'createlogin','status' => '1','created_at' => '2017-05-08 11:07:22','updated_at' => '2017-05-08 11:07:2'),

  		  array('id' => '6','parent_id' => '0','menu_name' =>'Enquiry','icon_img' => 'fa fa-question-circle','serial_no' => '0',
          'slug' => '','url' => 'enquiry','status' => '1','created_at' => '2017-05-08 11:07:22','updated_at' => '2017-05-08 11:07:22'),

       array('id' => '7','parent_id' => '0','menu_name' =>'Product','icon_img' => 'fa fa-product-hunt','serial_no' => '0','slug' => '',
        'url' => 'product','status' => '1','created_at' => '2017-05-08 11:07:22','updated_at' => '2017-05-08 11:07:22'),

       array('id' => '8','parent_id' => '0','menu_name' =>'Customer','icon_img' => 'fa fa-users','serial_no' => '0','slug' => '',
        'url' => 'customer','status' => '1','created_at' => '2017-05-08 11:07:22','updated_at' => '2017-05-08 11:07:22'),

        array('id' => '9','parent_id' => '8','menu_name' =>'Add new customer','icon_img' => 'fa fa-plus-circle','serial_no' => '0','slug' => '','url' => 'newcustomer','status' => '1','created_at' => '2017-05-08 11:07:22','updated_at' => '2017-05-08 11:07:22'),

        array('id' => '10','parent_id' => '8','menu_name' =>'Customer Details ','icon_img' => 'fa fa-eye','serial_no' => '0','slug' => '',
          'url' => 'customerdetails','status' => '1','created_at' => '2017-05-08 11:07:22','updated_at' => '2017-05-08 11:07:22'),

        array('id' => '11','parent_id' => '0','menu_name' =>'Product Mapping','icon_img' => 'fa fa-link','serial_no' => '0','slug' => '',
          'url' => 'productmapping','status' => '1','created_at' => '2017-05-08 11:07:22','updated_at' => '2017-05-08 11:07:22'),

        array('id' => '12','parent_id' => '0','menu_name' =>'Invoice','icon_img' => 'fa fa-print','serial_no' => '0','slug' => '',
          'url' => 'invoice','status' => '1','created_at' => '2017-05-08 11:07:22','updated_at' => '2017-05-08 11:07:22'),

        array('id' => '13','parent_id' => '0','menu_name' =>'Closure','icon_img' => 'fa fa-times-circle-o','serial_no' => '0','slug' => '',
          'url' => 'closure','status' => '1','created_at' => '2017-05-08 11:07:22','updated_at' => '2017-05-08 11:07:22'),

        array('id' => '14','parent_id' => '0','menu_name' =>'Report','icon_img' => 'fa fa-file-o','serial_no' => '0','slug' => '',
          'url' => 'report','status' => '1','created_at' => '2017-05-08 11:07:22','updated_at' => '2017-05-08 11:07:22'),

        array('id' => '15','parent_id' => '0','menu_name' =>'Notification','icon_img' => 'fa fa-bell','serial_no' => '0','slug' => '',
          'url' => 'notification','status' => '1','created_at' => '2017-05-08 11:07:22','updated_at' => '2017-05-08 11:07:22'),

        array('id' => '16','parent_id' => '0','menu_name' =>'Change password','icon_img' => 'fa fa-unlock-alt','serial_no' => '0','slug' => '','url' => 'change password','status' => '1','created_at' => '2017-05-08 11:07:22','updated_at' => '2017-05-08 11:07:22'),
  
        array('id' => '17','parent_id' => '0','menu_name' =>'Master Data','icon_img' => '','serial_no' => '0','slug' => '',
          'url' => 'masterdata','status' => '1','created_at' => '2017-06-16 11:07:22','updated_at' => '2017-06-16 11:07:22'),

        array('id' => '18','parent_id' => '17','menu_name' =>'Base Location','icon_img' => '','serial_no' => '0','slug' => '',
          'url' => 'baseloaction','status' => '1','created_at' => '2017-06-16 11:07:22','updated_at' => '2017-06-16 11:07:22'),

        array('id' => '19','parent_id' => '17','menu_name' =>'Product Category','icon_img' => 'fa fa-product-hunt','serial_no' => '0',
      'slug' => '','url' => 'productcategory','status' => '1','created_at' => '2017-06-16 11:07:22','updated_at' => '2017-06-16 11:07:22'),

        array('id' => '20','parent_id' => '17','menu_name' =>'Sub Category','icon_img' => '','serial_no' => '0','slug' => '',
          'url' => 'subcategory','status' => '1','created_at' => '2017-06-16 11:07:22','updated_at' => '2017-06-16 11:07:22'),

        array('id' => '21','parent_id' => '17','menu_name' =>'Brand','icon_img' => '','serial_no' => '0','slug' => '',
          'url' => 'brand','status' => '1','created_at' => '2017-06-16 11:07:22','updated_at' => '2017-06-16 11:07:22'),

         array('id' => '22','parent_id' => '17','menu_name' =>'Nativity','icon_img' => '','serial_no' => '0','slug' => '',
          'url' => 'nativity','status' => '1','created_at' => '2017-06-16 11:07:22','updated_at' => '2017-06-16 11:07:22'),

          array('id' => '23','parent_id' => '17','menu_name' =>'Mother Tongue','icon_img' => '','serial_no' => '0','slug' => '',
            'url' => 'mothertongue','status' => '1','created_at' => '2017-06-16 11:07:22','updated_at' => '2017-06-16 11:07:22'),

          array('id' => '24','parent_id' => '17','menu_name' =>'Area','icon_img' => 'fa fa-location-arrow','serial_no' => '0','slug' => '',
            'url' => 'area','status' => '1','created_at' => '2017-06-16 11:07:22','updated_at' => '2017-06-16 11:07:22'),

          array('id' => '25','parent_id' => '17','menu_name' =>'Vendor','icon_img' => 'fa fa-shopping-cart','serial_no' => '0','slug' => '',
            'url' => 'vendor','status' => '1','created_at' => '2017-06-16 11:07:22','updated_at' => '2017-06-16 11:07:22'),

          array('id' => '26','parent_id' => '17','menu_name' =>'Photo Proof ','icon_img' => 'fa fa-picture-o','serial_no' => '0','slug' => '',
            'url' => 'photoproof','status' => '1','created_at' => '2017-07-14 11:08:','updated_at' => '2017-07-14 11:07:22'),

          array('id' => '27','parent_id' => '17','menu_name' =>'Address Proof ','icon_img' => 'fa fa-address-card-o','serial_no' => '0','slug' => '','url' => 'addressproof','status' => '1','created_at' => '2017-07-14 11:07:22','updated_at' => '2017-07-14 11:07:22'),
        );

  		DB::table('menu')->insert($menu);	
    }
}
