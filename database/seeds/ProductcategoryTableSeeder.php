<?php

use Illuminate\Database\Seeder;

class ProductcategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('productcategory')->delete();
			$productcategory = array(
			  array('id' => '1','productcategory' => 'AC','status' => '1','created_by' => '','modified_by' => '','created_at' => '2017-06-24 11:45:11','updated_at' => '2017-06-29 09:17:41'),
			  array('id' => '2','productcategory' => 'TV','status' => '1','created_by' => '','modified_by' => '','created_at' => '2017-06-24 11:45:15','updated_at' => '2017-06-29 09:17:40'),
			  array('id' => '3','productcategory' => 'Refrigerator','status' => '1','created_by' => '','modified_by' => '','created_at' => '2017-06-28 09:56:10','updated_at' => '2017-06-28 09:56:10'),
			  array('id' => '4','productcategory' => 'Computer','status' => '1','created_by' => '','modified_by' => '','created_at' => '2017-06-29 11:48:33','updated_at' => '2017-07-07 07:28:11'),
			  array('id' => '5','productcategory' => 'Washing Machine','status' => '1','created_by' => '','modified_by' => '','created_at' => '2017-07-07 07:28:58','updated_at' => '2017-07-07 07:28:58')
			);

      	DB::table('productcategory')->insert($productcategory);	
    }
}
