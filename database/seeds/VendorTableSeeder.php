<?php

use Illuminate\Database\Seeder;

class VendorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    		 DB::table('vendor')->delete();

	    $vendor = array(
		  array('id' => '1','vendorname' => 'Saravana Stores','status' => '1','created_by' => '','modified_by' => '','created_at' => '2017-06-24 11:45:55','updated_at' => '2017-07-07 08:51:57'),
		  array('id' => '2','vendorname' => 'Vasanth and Co','status' => '0','created_by' => '','modified_by' => '','created_at' => '2017-07-07 07:38:29','updated_at' => '2017-07-07 08:52:03')
		);
	    DB::table('vendor')->insert($vendor);	

    }
}
