<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolePremissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role_permission', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('admin_role_id');
            // $table->foreign('admin_role_id')->references('id')->on('admin_roles');
            $table->integer('main_menu_id');
            // $table->foreign('main_menu_id')->references('id')->on('menu');
            $table->integer('sub_menu_id');
            // $table->foreign('sub_menu_id')->references('id')->on('menu');
            $table->integer('status');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('role_permission');
    }
}
