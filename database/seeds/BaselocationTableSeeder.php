<?php

use Illuminate\Database\Seeder;

class BaselocationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('baselocation')->delete();

			$baselocation = array(
			  array('id' => '1','baselocation' => 'Mandaveli','status' => '1','created_by' => '','modified_by' => '','created_at' => '2017-06-24 11:44:50','updated_at' => '2017-07-07 07:26:58'),
			  array('id' => '2','baselocation' => 'Mylapore','status' => '1','created_by' => '','modified_by' => '','created_at' => '2017-06-28 09:34:55','updated_at' => '2017-07-07 07:26:16'),
			  array('id' => '3','baselocation' => 'T Nagar','status' => '1','created_by' => '','modified_by' => '','created_at' => '2017-06-29 10:43:35','updated_at' => '2017-07-07 07:27:47')
			);

      	DB::table('baselocation')->insert($baselocation);	

    }

}
