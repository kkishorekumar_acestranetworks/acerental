<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('enquiry_id')->nullable();
            $table->string('name');
            $table->date('dob')->nullable();
            $table->string('gender');
            $table->string('mobile1')->unique();
            $table->string('mobile2')->unique()->nullable();
            $table->string('email1')->unique();
            $table->string('email2')->unique()->nullable();
            $table->string('nativity')->nullable();
            $table->string('marital_status')->nullable();
            $table->string('mother_tongue')->nullable();
            $table->string('rent_or_own')->nullable();
            $table->string('door_flatnum')->nullable();
            $table->string('floor_num')->nullable();
            $table->string('street')->nullable();
            $table->string('area')->nullable();
            $table->string('landmark')->nullable();
            $table->string('district');
            $table->string('state');
            $table->string('pincode')->nullable();
            // $table->string('p_rent_or_own')->nullable();
            $table->string('p_door_flatnum')->nullable();
            $table->string('p_floor_num')->nullable();
            $table->string('p_street')->nullable();
            $table->string('p_area')->nullable();
            $table->string('p_landmark')->nullable();
            $table->string('p_district');
            $table->string('p_state');
            $table->string('p_pincode')->nullable();
            $table->text('reference')->nullable();
            $table->string('companyname')->nullable();
            $table->string('desgination');
            $table->string('department');
            $table->string('companyemail')->nullable();
            $table->string('companyphno')->nullable();
            $table->string('companyaddress')->nullable();
            $table->string('customerphoto_path');
            $table->string('customerphoto');
            $table->text('id_proof')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('bank_branch')->nullable();
            $table->string('ifsc_code')->nullable();
            $table->string('account_num')->nullable();
            $table->string('upload_doument')->nullable();
            $table->string('upload_photo')->nullable();
            $table->string('remarks')->nullable();
            $table->string('attended')->nullable();
            $table->string('status');
            $table->string('created_by')->nullable();
            $table->string('modified_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
