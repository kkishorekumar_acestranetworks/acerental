<?php

use Illuminate\Database\Seeder;

class SubcategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
    	 DB::table('subcategory')->delete();
		 	$subcategory = array(
		  array('id' => '1','maincategory' => '1','subcategory' => '45 Kg','status' => '1','created_by' => '','modified_by' => '','created_at' => '2017-06-24 11:45:24','updated_at' => '2017-06-28 11:45:32'),
		  array('id' => '2','maincategory' => '3','subcategory' => '6.2 Kg','status' => '1','created_by' => '','modified_by' => '','created_at' => '2017-06-28 06:57:57','updated_at' => '2017-07-07 07:29:57'),
		  array('id' => '3','maincategory' => '5','subcategory' => '6.5 Kg','status' => '1','created_by' => '','modified_by' => '','created_at' => '2017-06-28 09:44:53','updated_at' => '2017-07-07 07:30:10'),
		  array('id' => '4','maincategory' => '5','subcategory' => '6 Kg','status' => '1','created_by' => '','modified_by' => '','created_at' => '2017-06-28 09:53:17','updated_at' => '2017-07-07 07:30:44'),
		  array('id' => '5','maincategory' => '1','subcategory' => 'Single Door','status' => '1','created_by' => '','modified_by' => '','created_at' => '2017-06-28 09:57:05','updated_at' => '2017-07-07 07:30:56'),
		  array('id' => '6','maincategory' => '1','subcategory' => '3 Star','status' => '1','created_by' => '','modified_by' => '','created_at' => '2017-06-28 11:43:08','updated_at' => '2017-07-07 07:31:44'),
		  array('id' => '7','maincategory' => '1','subcategory' => '5 Star','status' => '1','created_by' => '','modified_by' => '','created_at' => '2017-06-28 11:43:13','updated_at' => '2017-07-07 07:32:01'),
		  array('id' => '8','maincategory' => '2','subcategory' => '21','status' => '1','created_by' => '','modified_by' => '','created_at' => '2017-06-28 11:51:10','updated_at' => '2017-06-28 11:51:10'),
		  array('id' => '9','maincategory' => '2','subcategory' => '30','status' => '1','created_by' => '','modified_by' => '','created_at' => '2017-06-28 11:51:30','updated_at' => '2017-06-28 11:51:30'),
		  array('id' => '10','maincategory' => '1','subcategory' => '3 Star Split','status' => '1','created_by' => '','modified_by' => '','created_at' => '2017-06-28 12:02:22','updated_at' => '2017-07-07 07:32:53'),
		  array('id' => '11','maincategory' => '1','subcategory' => '5 Star Split','status' => '1','created_by' => '','modified_by' => '','created_at' => '2017-06-29 10:34:19','updated_at' => '2017-07-07 07:33:18')
		);
      	DB::table('subcategory')->insert($subcategory);	

    }
}
