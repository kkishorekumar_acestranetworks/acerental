<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnquiryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enquiry', function(Blueprint $table)
                {
                     
                        $table->increments('id');
                        $table->string('name');
                        $table->string('phone')->unique();
                        $table->string('location');
                        $table->string('email')->unique();
                        $table->string('source');   
                        $table->string('product');
                        $table->string('remarks');
                        $table->integer('status');
                        $table->timestamps();   
                                 
                });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('enquiry');

   }
}
