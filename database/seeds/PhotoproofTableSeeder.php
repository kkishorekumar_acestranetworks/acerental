<?php

use Illuminate\Database\Seeder;

class PhotoproofTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('photoproof')->delete();
      $photoproof = array(
		  array('id' => '1','photoproof_name' => 'Ration Card','status' => '1','created_by' => '','modified_by' => '','created_at' => '2017-07-28 12:11:51','updated_at' => '2017-06-28 12:11:51'),

		  array('id' => '2','photoproof_name' => 'Bank pass book','status' => '1','created_by' => '','modified_by' => '','created_at' => '2017-07-28 12:11:51','updated_at' => '2017-06-28 12:11:51'),

		  array('id' => '3','photoproof_name' => 'Aadhar Card No','status' => '1','created_by' => '','modified_by' => '','created_at' => '2017-07-28 12:11:51','updated_at' => '2017-06-28 12:11:51'),

		  array('id' => '4','photoproof_name' => 'Rental Agreement','status' => '1','created_by' => '','modified_by' => '','created_at' => '2017-07-28 12:11:51','updated_at' => '2017-06-28 12:11:51'),
		);
      DB::table('photoproof')->insert($photoproof);	
    }
}
