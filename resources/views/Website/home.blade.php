     @extends('websitemaster')

     @section('web_content')
     <div id="home" class="topbar">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 hidden-sm col-xs-4 dis"> <img id="myImage" class="img-responsive" src="img/acerental-logo.png" /> </div>
                    <div class="col-md-5 col-sm-8 hidden-xs ">
                        <p><i class="fa fa-bullhorn col"></i><span class="col faa-flash animated"> Get 15% off* every month | Hurry, offer expires soon! Grab the offer</span></p>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-8 text-right wid1">
                        <ul class="list-inline">
                            <li><a class="col" href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a class="col" href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a class="col" href="#"><i class="fa fa-google-plus"></i></a></li>
                            <a href="#"><img src="img/blog.png" style="width:13px;margin-left:0px;margin-right:0px;margin-top:-3px;"></a>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div id="owl-demo" class="owl-carousel owl-theme">
            <div class="item">
                <div class="pic pic1">
                    <div class="container">
                        <div class=" col-md-9 jumbotron text-left text-shadow col">
                            <h2>40' LED TV</h2>
                            <h1><b>Show Us What You Can Do..!</b></h1>
                            <h2><b> Rs. 3,600 (Refundable)</b></h2>
                            <p> Delivery and Installation: Free</p>
                            <a type="button" class="btn btn-success btn-responsive btn-lg"> <span> Order Now</span></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="pic pic2">
                    <div class="container">
                        <div class="col-md-9 jumbotron text-left text-shadow col">
                            <h2> 3 Seater Sofa</h2>
                            <h1><b>Choosing Furnitures for Your Home</b></h1>
                            <h2><b>Rs. 2,400 (Refundable)</b></h2>
                            <p> Delivery and Installation: Free</p>
                            <a type="button" class="btn btn-success btn-responsive btn-lg"> <span> Order Now</span></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="pic pic3">
                    <div class="container">
                        <div class="col-md-9 jumbotron text-left text-shadow col">
                            <h2>Refrigerator</h2>
                            <h1><b>Show Us What You Can Do..!</b></h1>
                            <h2><b> Rs. 3,600 (Refundable)</b></h2>
                            <p> Delivery and Installation: Free</p>
                            <a type="button" class="btn btn-success btn-responsive btn-lg"> <span> Order Now</span></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="pic pic4">
                    <div class="container">
                        <div class="col-md-9 jumbotron text-left text-shadow col">
                            <h2> Fitness Upright Bike</h2>
                            <h1><b>Are You Ready.. Sweat it Out?</b></h1>
                            <h2><b> Rs. 2,700 (Refundable)</b></h2>
                            <p> Delivery and Installation: Free</p>
                            <a type="button" class="btn btn-success btn-responsive btn-lg"> <span> Order Now</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

          <div class=" pos">
            <!-- <div class=" search">

                      <div class="input-group">
                          <input name="search_param" value="all" id="search_param" type="hidden">
                          <input class="form-control input-lg " name="x" placeholder="Search Item..." type="text">
                          <span class="input-group-btn">
                      <button class="btn btn-default btn-lg" type="button"><span class="glyphicon glyphicon-search"></span></button>
                          </span>
                      </div>
                  </div> -->
            <div class="hidden-xs">
                <div class="well well-transparent box-shadow ">
                    <div class="row ">
                        <form role="form">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label for="country"> Country</label>
                                    <select class="form-control">
                                        <option>France</option>
                                        <option>Indonesia</option>
                                        <option>Japan</option>
                                        <option>South Korea</option>
                                        <option>China</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="bedroom"> Rooms</label>
                                    <select class="form-control">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label for="location"> City</label>
                                    <select class="form-control">
                                        <option>Paris</option>
                                        <option>Yogyakarta</option>
                                        <option>Tokyo</option>
                                        <option>Busan</option>
                                        <option>Taipe</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="bathroom"> People</label>
                                    <select class="form-control">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label for="minprice"> Min Price</label>
                                    <select class="form-control">
                                        <option>$1,000</option>
                                        <option>$6,700</option>
                                        <option>$8,150</option>
                                        <option>$11,110</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label for="maxprice"> Max Price</label>
                                    <select class="form-control">
                                        <option>$8,000</option>
                                        <option>$11,700</option>
                                        <option>$14,150</option>
                                        <option>$21,110</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 mar">
                                <div class="form-group">
                                    <div class="input-group  ">
                                        <input placeholder="Search" class="form-control form-email-widget input-lg" type="text"> <span class="input-group-btn">
                                        <input value="✓" class="btn btn-primary btn-lg" type="submit">
                        </span> </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
         @include('Layout.menu')

         

        <div id="arrow-btn" class="pos2 text-center text-shadow"> <a class="btn btn-xl animated faa-bounce col" href="#categories" role="button"><i class="fa fa-angle-double-down fa-4x"></i></a> </div>
        <div id="categories" class="container">
            <div class="row  mar1 mar2 text-center">
                <div class="col-md-12 pad2">
                    <h1>CATEGORIES</h1> </div>
                <div class="col-md-3 col-sm-3 col-xs-12 width">
                    <div class="thumbnail hvr-sweep-to-top ">
                        <div class="col1">
                            <a href="#"> <img class="img-responsive" src="img/washing-machine.png" /> </a>
                            <div class="caption col">
                                <h4>APPLIANCES</h4>
                                <p><em>Claritas est etiam processus dynamicus</em></p>
                                <a href="Appliances.html" type="button" class="btn btn-success btn-responsive btn-lg "> <span> Browse Now</span></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12 width">
                    <div class="thumbnail hvr-sweep-to-top ">
                        <div class="col1">
                            <a href="#"> <img class="img-responsive" src="img/Furniture.png" /> </a>
                            <div class="caption col">
                                <h4>FURNITURE</h4>
                                <p><em>Claritas est etiam processus dynamicus</em></p>
                                <a type="button" class="btn btn-success btn-responsive btn-lg"> <span> Browse Now</span></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12 width">
                    <div class="thumbnail hvr-sweep-to-top ">
                        <div class="col1">
                            <a href="#"> <img class="img-responsive" src="img/Automobile.png" /> </a>
                            <div class="caption col">
                                <h4>AUTOMOBILE</h4>
                                <p><em>Claritas est etiam processus dynamicus</em></p>
                                <a type="button" class="btn btn-success btn-responsive btn-lg"> <span> Browse Now</span></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12 width">
                    <div class="thumbnail hvr-sweep-to-top ">
                        <div class="col1">
                            <a href="#"> <img class="img-responsive" src="img/Television.png" /> </a>
                            <div class="caption col">
                                <h4>ELECTRONICS</h4>
                                <p><em>Claritas est etiam processus dynamicus</em></p>
                                <a type="button" class="btn btn-success btn-responsive btn-lg"> <span> Browse Now</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="pic5">
            <div class="container">
                <div class="row pad pad2 text-center col">
                    <div class="col-md-12 pad3">
                        <h1>OUR LEADING BRANDS</h1> </div>
                    <div class="col-md-12">
                        <div id="owl-demo2" class="owl-carousel owl-theme pad2">
                            <div class="item ">
                                <div class="col-md-12 "> <img class="img-responsive box-shadow-hover center-block" src="img/brand1.jpg" /> </div>
                            </div>
                            <div class="item ">
                                <div class="col-md-12 "> <img class="img-responsive box-shadow-hover center-block" src="img/brand2.jpg" /> </div>
                            </div>
                            <div class="item ">
                                <div class="col-md-12 "> <img class="img-responsive box-shadow-hover center-block" src="img/brands3.jpg" /> </div>
                            </div>
                            <div class="item ">
                                <div class="col-md-12 "> <img class="img-responsive box-shadow-hover center-block" src="img/brand4.jpg" /> </div>
                            </div>
                            <div class="item ">
                                <div class="col-md-12 "> <img class="img-responsive box-shadow-hover center-block" src="img/brand5.jpg" /> </div>
                            </div>
                            <div class="item ">
                                <div class="col-md-12 "> <img class="img-responsive box-shadow-hover center-block" src="img/brand6.jpg" /> </div>
                            </div>
                            <div class="item ">
                                <div class="col-md-12 "> <img class="img-responsive box-shadow-hover center-block" src="img/brand7.jpg" /> </div>
                            </div>
                            <div class="item ">
                                <div class="col-md-12 "> <img class="img-responsive box-shadow-hover center-block" src="img/brand8.jpg" /> </div>
                            </div>
                            <div class="item ">
                                <div class="col-md-12 "> <img class="img-responsive box-shadow-hover center-block" src="img/brand9.jpg" /> </div>
                            </div>
                            <div class="item ">
                                <div class="col-md-12 "> <img class="img-responsive box-shadow-hover center-block" src="img/brand10.jpg" /> </div>
                            </div>
                            <div class="item ">
                                <div class="col-md-12 "> <img class="img-responsive box-shadow-hover center-block" src="img/brand11.jpg" /> </div>
                            </div>
                            <div class="item ">
                                <div class="col-md-12 "> <img class="img-responsive box-shadow-hover center-block" src="img/brand12.jpg" /> </div>
                            </div>
                            <div class="item ">
                                <div class="col-md-12 "> <img class="img-responsive box-shadow-hover center-block" src="img/brand13.jpg" /> </div>
                            </div>
                            <div class="item ">
                                <div class="col-md-12 "> <img class="img-responsive box-shadow-hover center-block" src="img/brand14.jpg" /> </div>
                            </div>
                            <div class="item ">
                                <div class="col-md-12 "> <img class="img-responsive box-shadow-hover center-block" src="img/brand15.jpg" /> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

          <div id="why_acerental" class="container ">
            <div class="row mar1 mar2 text-center ">
                <div class="col-md-12 pad2">
                    <h1>WHY ACERENTAL?</h1> </div>
                <div class="col-md-12">
                    <div id="owl-demo3" class="owl-carousel owl-theme ">
                        <div class="item">
                            <div class=" thumbnail hvr-sweep-to-top">
                                <div class="col-md-12 col-sm-12 col-xs-12  col1">
                                    <div class="col-md-6 col-sm-6 col-xs-12 wid ">
                                        <a class=""> <img class="img-responsive faa-bounce animated " src="img/Free-delivery.png"> </a>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12  wid pad6 pad2">
                                        <div class=" col">
                                            <h2 class="pad3">Free Delivery</h2>
                                            <p>Sit back, relax. Our delivery ninjas</p>
                                            <p> have got your back</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item ">
                            <div class="thumbnail hvr-sweep-to-top ">
                                <div class="col-md-12 col-sm-12 col-xs-12 col1 ">
                                    <div class="col-md-6 col-sm-6 col-xs-12  wid ">
                                        <a class=""> <img class="img-responsive faa-wrench animated   " src="img/Free-service.png"> </a>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12 wid pad6 pad2">
                                        <div class=" col">
                                            <h2 class="pad3">Free Service</h2>
                                            <p>Service is on us.</p>
                                            <p> Anytime. Anywhere</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="thumbnail hvr-sweep-to-top">
                                <div class="col-md-12 col-sm-12 col-xs-12 col1 ">
                                    <div class="col-md-6 col-sm-6 col-xs-12  wid ">
                                        <a class=""> <img class="img-responsive  faa-float animated faa-fast" src="img/Easy-pay.png"> </a>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12 wid pad6 pad2">
                                        <div class=" col">
                                            <h2 class="pad3">Easy Pay</h2>
                                            <p>Easily pay monthly rent online</p>
                                            <p>through Credit Card, Debit Card or Net Banking</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="thumbnail hvr-sweep-to-top">
                                <div class="col-md-12 col-sm-12 col-xs-12 col1 ">
                                    <div class="col-md-6 col-sm-6 col-xs-12  wid ">
                                        <a class=""> <img class="img-responsive  faa-horizontal animated " src="img/Free-pickup.png"> </a>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12 wid pad6 pad2 ">
                                        <div class="   col">
                                            <h2 class="pad3">Free Pickup</h2>
                                            <p>Free pickup</p>
                                            <p> Refund of deposit within 48 hours of pickup</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="pic6">
            <div class="container">
                <div class="row pad pad2 text-center col">
                    <div class="col-md-12 pad3">
                        <h1 class="font-size1">Service You Deserve. People You Trust.</h1>
                        <h3>We Care About Our Clients.</h3> </div>
                </div>
            </div>
        </div>

          <div id="how_it_works" class="container">
            <div class="row mar1 mar2 text-center">
                <div class="col-md-12 pad2 ">
                    <h1>HOW IT WORKS?</h1> </div>
                <div class="col-md-4 col-sm-4 col-xs-12 width2">
                    <div class="thumbnail hvr-sweep-to-top ">
                        <div class="col1  ">
                            <div class="col3 "> <span class="fa-stack fa-lg fa-5x  "><i class="fa fa-circle fa-stack-2x fa-inverse text-shadow"></i>
    <i class="fa fa-search fa-stack-1x faa-float animated "></i>

    </span> </div>
                            <div class="caption col">
                                <h4>FIND YOUR PRODUCT</h4>
                                <p><em>Search your Product or browse our categories to find the right product</em></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12 width2">
                    <div class="thumbnail hvr-sweep-to-top ">
                        <div class="col1">
                            <div class="col3"> <span class="fa-stack fa-lg fa-5x "><i class="fa fa-circle fa-stack-2x fa-inverse text-shadow"></i>
            <i class="fa fa-mobile fa-stack-1x faa-ring animated"></i>

            </span> </div>
                            <div class="caption col">
                                <h4>ENQUIRE WITH OWNER</h4>
                                <p><em>Talk to the Owner and finalize the deal for rental product</em></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12 width2">
                    <div class="thumbnail hvr-sweep-to-top">
                        <div class="col1">
                            <div class="col3"> <span class="fa-stack fa-lg fa-5x "><i class="fa fa-circle fa-stack-2x fa-inverse text-shadow"></i>
                    <i class="fa fa-shopping-cart fa-stack-1x   faa-horizontal animated  "></i>

                    </span> </div>
                            <div class="caption col">
                                <h4>RENT IT</h4>
                                <p><em>Pick up your product or get it delivered at your desktop</em></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection