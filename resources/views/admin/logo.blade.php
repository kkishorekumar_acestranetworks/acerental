@extends('admin.layout.auth')

@section('content')
<div class="container-fluid">
  <div class="col-md-2">
   <div class="sidebar-nav">
     <div class="well">
       <ul class="nav ">
         <li class="nav-header">Menu</li>
         <li class="active"><a href="/template/template">Template</a></li>
         <li class=""><a href="/template/logo">Logo</a></li>
      </ul>
     </div>
   </div>
 </div>

 <div class="col-md-10">

  <form class="form-horizontal" role="form" action="/template/logo_store" method="POST" enctype="multipart/form-data" >
    {{ csrf_field() }}

 <div class="panel panel-default">

   <div class="panel-heading">Add New Logo</div>

   <div class="panel-body">
<div class="col-md-6">
    <div class="form-group" >
      <label class="col-sm-3 control-label" for="name">Enter Logo Name</label>
      <div class="col-sm-9">
       <input type="text" class="form-control" name="name" id="name" value="<?php if(isset($data)) { echo $data->name; }   ?>" placeholder="Enter Logo Name">
     </div>
    </div>
    </div>
<div class="col-md-6">
    <div class="form-group">
      <label class="col-sm-3 control-label" for="url">Rate</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" name="rate" id="rate" value="<?php if(isset($data)) { echo $data->rate; } ?>" placeholder="Enter Rate">
     </div>
    </div>
    </div>

<div class="col-md-6">
     <div class="form-group">
      <label class="col-sm-3 control-label" for="subname">Sulg</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" name="slug" value="<?php if(isset($data)) { echo $data->slug; } ?>" id="slug" placeholder="Enter Slug">
       </div>
    </div>
    </div>

<div class="col-md-6">
    <div class="form-group">
      <label class="col-sm-3 control-label"  for="logo_image">Logo Image</label>
      <div class="col-md-9">
      <input type="file" class="form-control-file btn btn-default" value="image" id="image_name" name="image">

      @if(isset($data->id))
      <div class="img-wrap col-md-offset-4">
      
   
      <span class="close" value="<?php echo $data->id; ?>">x</span>
         <img class="img-thumbnail" src="<?php if(isset($data)) { echo '/'.$data->image_path .'/'. $data->image; } ?>" style="height:100px;width: 100px;">
      </div>
  @endif
     </div>
   </div></div>

      <input  type="hidden" name="id" value="<?php if(isset($data)) { echo $data->id; } ?>"/>
 
 </div>

    <div class="form-group">
      <div class="col-sm-offset-3 col-sm-9">
        <button type="submit" class="btn btn-success">Submit</button>
        <button type="button" class="btn btn-danger">Reset</button>
      </div>
    </div>


    </div>
      </form>



<div class="">
    <div class="panel panel-default">
      <div class="panel-heading">View Logo List</div>
      <div class="panel-body">
        <div class="col-md-offset-8 ">
          <div class="form-group">
          </div>
        </div>

        <table class="table table-bordered table-hover">

          <thead>
          <tr>
          <th>Id </th>
          <th>Logo Name</th>
          <th>Rate</th>
          <th>Slug</th>
          <th>Action</th>
          <th>Status</th></tr>
          <tr>
          @if(isset($logo_list))

          @foreach($logo_list as $row)

               <td>  {{ $row->id }}  </td>
               <td>  {{ $row->name }}</td>
               <td>  {{ $row->rate }}</td>
               <td>  {{ $row->slug }}</td>

               <td> 

              <a href="/template/logo_edit/{{ $row->id }}"> <button type="submit"  class="btn  btn-xs glyphicon glyphicon-pencil ">Edit</button></a>
              <a href="/template/delete_logo/{{ $row->id }}"><button type="submit" class="btn  btn-xs glyphicon glyphicon-trash ">Delete</button></a></td>

               <td> <a href=""><button type="button" class="btn btn-warning btn-xs">InActive</button></a>
                    <a href=""> <button type="button" class="btn btn-success btn-xs">Active</button></a></td>
               </tr>

            @endforeach
            @endif
       
          </thead>

          </table>
           <div class="text-center">
          <div class="pagination ">
 <?php echo $logo_list->render(); ?>
</div></div>
       </div>
    </div>
  </div>
  </div>

   </div>
  
@endsection
