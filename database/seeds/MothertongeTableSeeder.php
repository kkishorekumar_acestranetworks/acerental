<?php

use Illuminate\Database\Seeder;

class MothertongeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('mothertongue')->delete();
      $mothertongue = array(
		  array('id' => '1','mothertongue' => 'Tamil','status' => '1','created_by' => '','modified_by' => '','created_at' => '2017-07-07 07:37:22','updated_at' => '2017-07-07 07:37:22'),
		  array('id' => '2','mothertongue' => 'English','status' => '1','created_by' => '','modified_by' => '','created_at' => '2017-07-07 07:37:26','updated_at' => '2017-07-07 07:37:26'),
		  array('id' => '3','mothertongue' => 'Hindi','status' => '1','created_by' => '','modified_by' => '','created_at' => '2017-07-07 07:37:38','updated_at' => '2017-07-07 07:37:38')
		);
		DB::table('mothertongue')->insert($mothertongue);	
      

    }
}
