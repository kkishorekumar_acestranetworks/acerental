<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Acerental</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/custom.css">
  
    <!--animation-->
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <!-- Owl Carousel Assets -->
    <link href="css/owl.carousel.min.css" rel="stylesheet">
        <link href="css/owl.theme.default.min.css" rel="stylesheet">
    <!-- Awesome Font -->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome/css/font-awesome-animation.css">

    <body data-spy="scroll" data-target=".navbar-default">
   
        
    

      @yield('web_content')
       
        <div id="need_help" class="col2">
            <div class="container">
                <div class="row pad pad2 text-center col">
                    <div class="col-md-12 pad3">
                        <h1>NEED HELP?</h1> </div>
                    <div class="col-md-12">
                        <h3>Call Us</h3>
                        <h1 class="font-size1">(080) 39511847</h1>
                        <h3>(Mon - Fri: 10am to 8pm)</h3>
                        <a type="button" href="FAQ.html" class="btn btn-primary btn-responsive btn-lg mar3"> <span> Check FAQ</span></a>
                    </div>
                </div>
            </div>
        </div>
        <div id="footer" class="col1 ">
            <div class="container footer1">
                <div class="row mar2">
                    <div class="col-md-3  col-sm-4 col-xs-12 mar1 wid">
                        <a class="" href="#"><img id="myImage" class="img-responsive center-block mar1" src="img/acerental-logo.png" /> </a>
                        <div class="text-center ">
                            <p><i class="fa fa-envelope-o col pad4"></i><a href="#" class="col">rent@acerental.com</a></p>
                            <p><i class="fa fa-phone col pad4"></i><a href="#" class="col">044 3100 3040</a></p>
                            <p><i class="fa fa-phone col pad4"></i><a href="#" class="col">044 3100 4050</a></p>
                        </div>
                    </div>
                    <div class="col-md-2 col-md-offset-1 col-sm-2 col-xs-6 mar1 wid">
                        <h4 class="pad3 col3">PRODUCTS</h4>
                        <p><a class="col" href="#">  Appliances</a> </p>
                        <p><a class="col" href="#"> Furniture</a> </p>
                        <p><a class="col" href="#"> Automobile</a> </p>
                        <p><a class="col" href="#"> Electronics</a> </p>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-6 mar1 wid">
                        <h4 class="pad3 col3">ACERENTAL</h4>
                        <p><a class="col" href="/aboutus"> About Us</a> </p>
                        <p><a class="col" href="/contactus"> Contact Us</a> </p>
                        <p><a class="col" href="#"> Blog</a> </p>
                        <p><a class="col" href="/faq"> FAQ</a> </p>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-6 mar1 wid">
                        <h4 class="pad3 col3">POLICIES</h4>
                        <p><a class="col" href="/privacypolicy"> Privacy Policy</a> </p>
                        <p><a class="col" href="/termsofuse "> Terms of Use</a> </p>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-6 mar1 wid">
                        <a class="" href="#"><img id="myImage" class="img-responsive center-block mar1" src="img/qr-code.jpg" /> </a>
                    </div>
                </div>
            </div>
            <hr>
            <div class="footer2 container">
                <div class="nav navbar-nav">
                    <div class="socials "><a href="#"><i class="col fa fa-facebook"></i></a> <a href="#"><i class="col fa fa-twitter"></i></a> <a href="#"><i class=" col fa fa-google-plus"></i></a>
                        <a href="#"><img src="img/blog.png" style="width:13px;margin-left:0px;margin-right:0px;margin-top:-3px;"></a>
                    </div>
                </div>
                <div class="navbar-right">
                    <p class="text-muted "><strong class="pad4">© Copyright 2017</strong><a href="#" class="text-muted">AceRental.com</a></p>
                </div>
            </div>
            <div id="user-comment" class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
                                              href="#collapse-200">
                                        Need Help?
                                            </a>
                                          </h4> </div>
                <div id="collapse-200" class="panel-collapse collapse">
                    <div class="panel-body">
                        <p> We would love to talk to you. But first, please tell us a bit about yourself.</p>
                        <div class="col-md-7 col-sm-7 col-xs-12">
                            <div class="form-group">
                                <div class="">
                                    <input type="text" class="form-control" id="ref-name1" placeholder="Username"> </div>
                            </div>
                            <div class="form-group">
                                <div class="">
                                    <input type="text" class="form-control" id="ref-email-id1" placeholder="Email"> </div>
                            </div>
                            <div class="form-group">
                                <div class="">
                                    <input type="text" class="form-control" id="ref-ph-no 1" placeholder="phoneno"> </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 hidden-xs"> <span class="fa-stack fa-lg fa-5x  "><i class="fa fa-circle fa-stack-2x fa-inverse text-shadow faa-burst animated "></i>
                                          <i class="fa fa-question-circle fa-stack-1x col3 faa-pulse animated "></i>

                                            </span> </div>
                    </div>
                    <div class="panel-footer clearfix">
                        <div class="btn-group btn-group-md pull-left"><a class="btn btn-danger" href="#">Clear</a></div>
                        <div class="btn-group btn-group-md pull-right"><a class="btn btn-primary" href="#">Submit</a></div>
                    </div>
                </div>
            </div>
        </div>
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/owl.carousel.min.js" type="text/javascript"></script>
        <script src="js/TweenMax.min.js"></script>
        <script src="js/ScrollToPlugin.min.js"></script>
        <script src="js/custom.js" type="text/javascript"></script>
        <script>
        </script>
    </body>

</html>