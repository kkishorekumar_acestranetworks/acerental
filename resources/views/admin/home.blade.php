@extends('admin.layout.auth')

@section('content')
<div class="container-fluid">
  <div class="col-md-2">
   <div class="sidebar-nav">
     <div class="well">
       <ul class="nav ">
         <li class="nav-header">Menu</li>
         <li class="active"><a href="/template/template">Template</a></li>
         <li class=""><a href="/template/logo">Logo</a></li>
      </ul>
     </div>
   </div>
 </div>

 <div class="col-md-10">

  <form class="form-horizontal" role="form" action="/template/store" method="POST" enctype="multipart/form-data" >
    {{ csrf_field() }}

    <div class="panel panel-default">
   <div class="panel-heading">Add New Template</div>
    <div class="panel-body">
    <div class="col-md-6">
    <div class="form-group" >
      <label class="col-sm-3 control-label" for="name">Name</label>
      <div class="col-sm-9">
      <input type="text" class="form-control" name="name" id="name"  value="<?php if(isset($data)) { echo $data->name; } ?>" placeholder="Name">
       </div>
    </div></div>

 <div class="col-md-6">
    <div class="form-group">
      <label class="col-sm-3 control-label" for="url">URL</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" name="url" value="<?php if(isset($data)) { echo $data->url; } ?>" id="url" placeholder="Enter Url">

      </div>
    </div></div>

 <div class="col-md-6">
     <div class="form-group">
      <label class="col-sm-3 control-label" for="subname">Sub Name</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" name="subname" value="<?php if(isset($data)) { echo $data->subname; } ?>" id="subname"
        placeholder="Enter Sub Name">

      </div>
    </div></div>
 <div class="col-md-6">
    <div class="form-group">

      <label class="col-sm-3 control-label" for="description">Description</label>
      <div class="col-sm-9">
        <textarea class="form-control" name="description"  id="description"><?php if(isset($data)) { echo $data->description; } ?></textarea>
    </div>
    </div></div>
    
 <div class="col-md-6">
    <div class="form-group">
      <label class="col-sm-3 control-label"  for="mobile_image">Mobile Screenshots</label>
      <div class="col-md-9">
           <input type="file" class="form-control-file btn btn-default" value="image" id="mobile_image" name="mobile_image">
        </div>

    </div>
    <div class="text-center"> 
    @if(isset($data->id))
       <div class="img-wrap">
      <span class="close">&times;</span>
        <img class="img-thumbnail" src="<?php if(isset($data)) { echo '/'.$data->mobile_image_path .'/'. $data->mobile_image; } ?>" style="height:100px;width: 100px;"  >
      </div>
       @endif </div>
    </div>

     <div class="col-md-6">
    <div class="form-group">
      <label class="col-sm-3  control-label"  for="image">Main Screenshots</label>
      <div class="col-md-9">
      <input  type="file" class="form-control-file btn btn-default"  id="image" name="image[]" multiple>
@if(isset($data->id))

     @foreach($images as $images)
     <a href="/template/delete_image/{{$images->id}}">
        <div class="img-wrap ">
        
      <span class="close">&times;</span>
     <img class="img-thumbnail" src="<?php if(isset($data)) { echo '/'.$images->image_path .'/'. $images->image; } ?>" style="height:100px;width: 100px;"></a>
      </div>
      @endforeach
  @endif

      </div>
  </div></div>

  <input type="hidden" name="id" value="<?php if(isset($data)) { echo $data->id; } ?>"/>

 <div class="col-md-6">
    <div class="form-group">
      <label class="col-sm-3 control-label" for="category" ">Category</label>
        <div class="col-xs-3">
            <select class="form-control" name="category" value="<?php if(isset($data)) { echo $data->category; } ?>">

                <option value="dynamic" selected>Dynamic</option>
                <option value="wordpress" selected>WordPress</option>
                <option value="static" selected>Static</option>
                <option value="app" selected>App</option>
                <option value="dashboard" >Dashboard</option>

          </select>
      </div>


    </div></div>

    <div class="form-group">
      <div class="col-sm-offset-3 col-sm-9">
        <button type="submit" class="btn btn-success">Submit</button>
      </div>
    </div>
    </div>
    </form>
    </div>






  </div>




@endsection
